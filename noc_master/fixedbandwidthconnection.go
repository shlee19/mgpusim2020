package noc

import (
	"log"
	"reflect"
	"sync"

	"gitlab.com/akita/akita"
)

// FixedBandwidthConnection models a connection that transfers messages at a
// certain byte per cycle speed.
type FixedBandwidthConnection struct {
	sync.Mutex
	akita.HookableBase

	engine akita.Engine
	ticker *akita.Ticker

	BytesPerCycle int
	NumLanes      int
	busy          int

	Freq              akita.Freq
	SrcBufferCapacity int
	DstBufferCapacity int

	srcBuffers         []*MsgBuffer
	srcPortToBufferMap map[akita.Port]*MsgBuffer
	srcBufferBusy      map[*MsgBuffer]bool
	lastSelectedSrcBuf int

	dstBuffers         []*MsgBuffer
	dstBufferToPortMap map[*MsgBuffer]akita.Port
	dstPortToBufferMap map[akita.Port]*MsgBuffer
	dstBusy            map[akita.Port]bool
	dstNumArrivingMsgs map[akita.Port]int

	needTick bool
}

// Handle handles events scheduled on the FixedBandwidthConnection
func (c *FixedBandwidthConnection) Handle(e akita.Event) error {
	now := e.Time()
	ctx := akita.HookCtx{
		Domain: c,
		Now:    now,
		Pos:    akita.HookPosBeforeEvent,
		Item:   e,
	}
	c.InvokeHook(&ctx)

	c.Lock()

	switch evt := e.(type) {
	case akita.TickEvent:
		c.tick(now)
	case *TransferEvent:
		c.doTransfer(evt)
	default:
		log.Panicf("cannot handle event of type %s", reflect.TypeOf(evt))
	}

	c.Unlock()

	ctx.Pos = akita.HookPosAfterEvent
	c.InvokeHook(&ctx)

	return nil
}

func (c *FixedBandwidthConnection) tick(now akita.VTimeInSec) {
	c.needTick = false

	c.doDeliver(now)
	c.scheduleTransfer(now)

	if c.needTick {
		c.ticker.TickLater(now)
	}
}

func (c *FixedBandwidthConnection) doDeliver(now akita.VTimeInSec) {
	for _, buf := range c.dstBuffers {
		dst := c.dstBufferToPortMap[buf]

		if c.dstBusy[dst] {
			continue
		}

		if len(buf.Buf) == 0 {
			continue
		}

		msg := buf.Buf[0]
		msg.Meta().RecvTime = now
		err := dst.Recv(msg)
		if err == nil {
			buf.Buf = buf.Buf[1:]
			c.needTick = true

			ctx := akita.HookCtx{
				Domain: c,
				Now:    now,
				Pos:    akita.HookPosConnDeliver,
				Item:   msg,
			}
			c.InvokeHook(&ctx)

			// log.Printf("%.15f: msg %s delivered", now, msg.GetID())
		} else {
			c.dstBusy[dst] = true
		}
	}
}

func (c *FixedBandwidthConnection) scheduleTransfer(now akita.VTimeInSec) {
	if c.busy >= c.NumLanes {
		return
	}

	for i := 0; i < len(c.srcBuffers); i++ {

		bufIndex := (i + c.lastSelectedSrcBuf + 1) % len(c.srcBuffers)

		buf := c.srcBuffers[bufIndex]
		if c.srcBufferBusy[buf] {
			continue
		}
		if len(buf.Buf) == 0 {
			continue
		}

		msg := buf.Buf[0]
		dst := msg.Meta().Dst
		dstBuf := c.dstPortToBufferMap[dst]
		if len(dstBuf.Buf)+c.dstNumArrivingMsgs[dst] >= c.DstBufferCapacity {
			continue
		}

		c.needTick = true
		cycles := ((msg.Meta().TrafficBytes - 1) / c.BytesPerCycle) + 1
		transferTime := c.Freq.NCyclesLater(cycles, now)
		transferEvent := NewTransferEvent(transferTime, c, msg, 0)
		c.dstNumArrivingMsgs[dst]++
		c.engine.Schedule(transferEvent)

		ctx := akita.HookCtx{
			Domain: c,
			Now:    now,
			Pos:    akita.HookPosConnStartTrans,
			Item:   msg,
		}
		c.InvokeHook(&ctx)

		c.busy++

		c.srcBufferBusy[buf] = true
		c.lastSelectedSrcBuf = bufIndex
	}
}

func (c *FixedBandwidthConnection) doTransfer(evt *TransferEvent) {
	now := evt.Time()
	msg := evt.msg
	src := msg.Meta().Src
	dst := msg.Meta().Dst

	srcBuf := c.srcPortToBufferMap[src]
	dstBuf := c.dstPortToBufferMap[dst]

	srcBuf.Buf = srcBuf.Buf[1:]
	dstBuf.Buf = append(dstBuf.Buf, msg)

	c.busy--
	c.srcBufferBusy[srcBuf] = false
	c.dstNumArrivingMsgs[dst]--
	c.ticker.TickLater(now)

	// log.Printf("%.15f: msg %s transferred", now, msg.GetID())

	ctx := akita.HookCtx{
		Domain: c,
		Now:    now,
		Pos:    akita.HookPosConnDoneTrans,
		Item:   msg,
	}
	c.InvokeHook(&ctx)

	src.NotifyAvailable(evt.Time())
}

// Send start the transfer of the request. It guarantees that the message get
// delievered sometime later. It may also return an error indicating that this
// message cannot be sent at the moment.
func (c *FixedBandwidthConnection) Send(msg akita.Msg) *akita.SendError {
	c.Lock()
	defer c.Unlock()

	buf := c.srcPortToBufferMap[msg.Meta().Src]

	if buf == nil {
		log.Panic("not connected")
	}

	if len(buf.Buf) >= c.SrcBufferCapacity {
		return akita.NewSendError()
	}
	//msg.Meta().TrafficBytes = 64
	//log.Printf("%.15f: %s -> %s msg start sending, Size : %d ", msg.Meta().SendTime, msg.Meta().Src.Component().Name(), msg.Meta().Dst.Component().Name(), msg.Meta().TrafficBytes)

	buf.Buf = append(buf.Buf, msg)
	c.ticker.TickLater(msg.Meta().SendTime)

	ctx := akita.HookCtx{
		Domain: c,
		Now:    msg.Meta().SendTime,
		Pos:    akita.HookPosConnStartSend,
		Item:   msg,
	}
	c.InvokeHook(&ctx)

	return nil
}

// PlugIn connects a port to the FixedBandwidthConnection
func (c *FixedBandwidthConnection) PlugIn(port akita.Port) {
	_, connected := c.srcPortToBufferMap[port]
	if connected {
		log.Panic("port already connected")
	}

	srcBuf := &MsgBuffer{
		Capacity: c.SrcBufferCapacity,
	}
	c.srcBuffers = append(c.srcBuffers, srcBuf)
	c.srcPortToBufferMap[port] = srcBuf
	c.srcBufferBusy[srcBuf] = false

	dstBuf := &MsgBuffer{
		Capacity: c.DstBufferCapacity,
	}
	c.dstBuffers = append(c.dstBuffers, dstBuf)
	c.dstBufferToPortMap[dstBuf] = port
	c.dstPortToBufferMap[port] = dstBuf
	c.dstNumArrivingMsgs[port] = 0
	c.dstBusy[port] = false

	port.SetConnection(c)
}

// Unplug disconnect the connection with a port
func (FixedBandwidthConnection) Unplug(port akita.Port) {
	panic("implement me")
}

// NotifyAvailable enables the FixedBandwidthConnection to continue try to
// deliver to a desination
func (c *FixedBandwidthConnection) NotifyAvailable(
	now akita.VTimeInSec,
	port akita.Port,
) {
	c.Lock()
	c.dstBusy[port] = false
	c.ticker.TickLater(now)
	c.Unlock()
}

// NewFixedBandwidthConnection creates a new FixedBandwidthConnection
func NewFixedBandwidthConnection(
	bytesPerCycle int,
	engine akita.Engine,
	freq akita.Freq,
) *FixedBandwidthConnection {
	conn := new(FixedBandwidthConnection)

	conn.BytesPerCycle = bytesPerCycle
	conn.SrcBufferCapacity = 1
	conn.DstBufferCapacity = 1
	conn.srcPortToBufferMap = make(map[akita.Port]*MsgBuffer)
	conn.srcBufferBusy = make(map[*MsgBuffer]bool)
	conn.dstBufferToPortMap = make(map[*MsgBuffer]akita.Port)
	conn.dstPortToBufferMap = make(map[akita.Port]*MsgBuffer)
	conn.dstNumArrivingMsgs = make(map[akita.Port]int)
	conn.dstBusy = make(map[akita.Port]bool)

	conn.engine = engine
	conn.Freq = freq
	conn.ticker = akita.NewTicker(conn, engine, conn.Freq)

	conn.NumLanes = 1

	return conn
}
