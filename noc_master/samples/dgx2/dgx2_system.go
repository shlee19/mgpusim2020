package main

import (
	"fmt"
	//	"math"

	"gitlab.com/akita/noc"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/noc/standalone"
)

func main() {
	iterNums := 25
	for q := 0; q < iterNums; q++ {
		rengine := akita.NewSerialEngine()

		//rengine.AcceptHook(akita.NewEventLogger(log.New(os.Stdout, "", 0)))

		trafficInjector := standalone.NewGreedyTrafficInjector(rengine)
		trafficInjector.PackageSize = 64

		//trafficCounter := new(noc.TrafficCounter)
		//conn := noc.NewFixedBandwidthConnection(16, engine, 1*akita.GHz)

		InputDegree := 9
		RouterVC := 9
		router := noc.NewSimpleRouter("router", InputDegree, InputDegree, 1, rengine, 1*akita.GHz)
		conn := noc.NewRouterConnection(rengine, router, RouterVC)
		conn.RouterInputDegree = InputDegree

		router.RoutingTable = make(map[akita.Port]int)

		var agents []*standalone.Agent
		for i := 0; i < 16; i++ {
			name := fmt.Sprintf("GPU_%d.L2TLB", i)
			agent := standalone.NewAgent(name, rengine)
			trafficInjector.RegisterAgent(agent)
			conn.PlugIn(agent.ToOut)

			agents = append(agents, agent)

			name = fmt.Sprintf("GPU_%d", i)
			agent = standalone.NewAgent(name, rengine)
			trafficInjector.RegisterAgent(agent)
			conn.PlugIn(agent.ToOut)

			agents = append(agents, agent)

			name = fmt.Sprintf("GPU_%d.RDMA", i)
			agent = standalone.NewAgent(name, rengine)
			trafficInjector.RegisterAgent(agent)
			conn.PlugIn(agent.ToOut)

			agents = append(agents, agent)
		}

		name := "driver"
		agent := standalone.NewAgent(name, rengine)
		trafficInjector.RegisterAgent(agent)
		conn.PlugIn(agent.ToOut)

		agents = append(agents, agent)

		name = "MMU"
		agent = standalone.NewAgent(name, rengine)
		trafficInjector.RegisterAgent(agent)
		conn.PlugIn(agent.ToOut)

		agents = append(agents, agent)

		conn.BuildDGX2_Network()
		conn.ConnectingDGX2_Network()
		conn.DGX2_TopologyCheck()

		trafficInjector.InjectRate = 1 + q
		trafficInjector.InjectDGX_2_Traffic()

		rengine.Run()

		totalData := conn.TotalDataReport()

		//temp := float64(totalData) / float64(1024*1024*1024) // Bytes to GBytes
		//temp := float64(totalData) / float64(1e9)
		//BW := float64(temp) / float64(rengine.CurrentTime())

		total_time := float64(rengine.CurrentTime())

		fmt.Printf("It takes %0.12f  seconds \n", total_time)

		//fmt.Printf("Bandwidth achieved %0.12f GB/s\n", BW)
		//fmt.Printf("Utilization %0.12f percentage \n", (BW/float64(numAgents*64))*float64(100))
		fmt.Printf("Total data sent over interconnect %d Bytes\n", totalData)
		fmt.Printf("=============   Rate : %d  =========== \n", 1+q)
		conn.AveragePacketLatencyReport()

		//fmt.Printf("Bandwidth achieved %f GB/s\n", float64(262144*4*64)/float64(rengine.CurrentTime())/1e9)
		//fmt.Printf("Bandwidth achieved %0.12f GB/s\n", float64(totalData)/float64(rengine.CurrentTime())/1e9)

		//fmt.Printf("Average Packet Latency is %0.12f \n", conn.AveragePacketLatencyReport())

	}
}
