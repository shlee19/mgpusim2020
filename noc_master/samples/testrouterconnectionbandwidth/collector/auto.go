package main

import (
	"flag"
	"fmt"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/noc"
	"gitlab.com/akita/noc/standalone"
)

var InjectRate = flag.Int("InjectRate", 1, "InjectRate")
var dgxflag = flag.Int("dgxflag", 1, "DGX FLAG")

func main() {
	iterNums := 1
	flag.Parse()

	for q := 0; q < iterNums; q++ {
		rengine := akita.NewSerialEngine()
		VarInject := *InjectRate
		fmt.Printf("Inject : %d \n", VarInject)
		VarInject = *InjectRate
		trafficInjector := standalone.NewGreedyTrafficInjector(rengine)
		trafficInjector.PackageSize = 128

		//FlitSize := 32
		numGPUs := 8

		numRouterVC := 1 // Number of Virtual Channels
		RouterFreq := 1 * akita.GHz

		conn := noc.NewRouterConnection(rengine, numGPUs, numRouterVC, RouterFreq)
		conn.WarmUpCycle = 500
		conn.DrainCycle = 1000
		conn.NumGPUs = numGPUs
		trafficInjector.NumGPUs = numGPUs

		conn.FlitSize = 16
		conn.RouterBufferSize = 20

		//	rengine.RegisterSimulationEndHandler(NocLogger)

		// DGX-1

		if *dgxflag == 1 {
			PlaneDegree := 4
			Stack := 2
			conn.BuildScalable_DGX1_Network(numGPUs, PlaneDegree, Stack)
			conn.ConnectingScalable_DGX1_Network(numGPUs, PlaneDegree, Stack)
			conn.IsDGX1 = true
			conn.Argv1 = PlaneDegree
			conn.Argv2 = Stack
		}

		if *dgxflag == 2 {
			numSwitchs := 6
			conn.Argv1 = numSwitchs
			conn.BuildScalable_DGX2_Network(numGPUs, numSwitchs)
			conn.ConnectingScalable_DGX2_Network(numGPUs, numSwitchs)
			conn.IsDGX2 = true
		}

		// DGX-2

		/*
			numSwitchs := 6
			conn.Argv1 = numSwitchs
			conn.BuildScalable_DGX2_Network(numGPUs, numSwitchs)
			conn.ConnectingScalable_DGX2_Network(numGPUs, numSwitchs)
			conn.IsDGX2 = true
		*/

		// 4 DGX
		var agents []*standalone.Agent

		for i := 1; i <= numGPUs; i++ {
			name := fmt.Sprintf("GPU_%d", i)
			agent := standalone.NewAgent(name, rengine)
			trafficInjector.RegisterAgent(agent)
			conn.PlugIn(agent.ToOut)

			name2 := fmt.Sprintf("GPU_%d.DMA", i)
			agent2 := standalone.NewAgent(name2, rengine)
			trafficInjector.RegisterAgent(agent2)
			conn.PlugIn(agent2.ToOut)

			name3 := fmt.Sprintf("GPU_%d.DMA", i)
			agent3 := standalone.NewAgent(name3, rengine)
			trafficInjector.RegisterAgent(agent3)
			conn.PlugIn(agent3.ToOut)

			agents = append(agents, agent)
			agents = append(agents, agent2)
			agents = append(agents, agent3)
		}

		driver := fmt.Sprintf("driver")
		a_dri := standalone.NewAgent(driver, rengine)
		trafficInjector.RegisterAgent(a_dri)
		conn.PlugIn(a_dri.ToOut)

		mmu := fmt.Sprintf("MMU")
		a_mmu := standalone.NewAgent(mmu, rengine)
		trafficInjector.RegisterAgent(a_mmu)
		conn.PlugIn(a_mmu.ToOut)

		trafficInjector.InjectRate = VarInject
		//trafficInjector.InjectRate = 1000
		trafficInjector.InjectP2PTraffic()
		start := rengine.CurrentTime()
		rengine.Run()
		//rengine.Finished()
		end := rengine.CurrentTime()

		fmt.Printf("Inject Rate: %d \n", VarInject)

		fmt.Printf("CheolGyu TIME: %0.12f \n", end-start)

		fmt.Printf("Total Flits : %d (expected : %d ) \n", conn.Counter4Debug, (numGPUs*3+2)*4*trafficInjector.NumPackages)
	}
}
