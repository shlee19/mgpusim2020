package noc

import (
	"log"
	"sync"

	"reflect"

	"gitlab.com/akita/akita"
)

// MsgPool is a pool of messages.
type MsgPool struct {
	Pool           []*MsgBuffer
	lastSelectedVC int
}

// PCIeSwitchConnection models a PCIe interconnect with a single switch
type PCIeSwitchConnection struct {
	sync.Mutex
	akita.HookableBase

	engine akita.Engine
	ticker *akita.Ticker

	BytesPerCycle int
	NumLanes      int
	busy          int

	Freq              akita.Freq
	SrcBufferCapacity int
	DstBufferCapacity int
	totalVC           int

	srcPools           []*MsgPool
	srcPortToPoolMap   map[akita.Port]*MsgPool
	srcBufferBusy      map[*MsgBuffer]bool
	lastSelectedSrcBuf int

	dstBuffers         []*MsgBuffer
	dstBufferToPortMap map[*MsgBuffer]akita.Port
	dstPortToBufferMap map[akita.Port]*MsgBuffer
	dstBusy            map[akita.Port]bool
	dstNumArrivingMsgs map[akita.Port]int

	needTick bool
}

// Handle defines how the PCIeSwitchConnections handles events
func (c *PCIeSwitchConnection) Handle(e akita.Event) error {
	now := e.Time()
	ctx := akita.HookCtx{
		Domain: c,
		Now:    now,
		Pos:    akita.HookPosBeforeEvent,
		Item:   e,
	}
	c.InvokeHook(&ctx)

	c.Lock()

	switch evt := e.(type) {
	case akita.TickEvent:
		c.tick(evt.Time())
	case *TransferEvent:
		c.doTransfer(evt)
	default:
		log.Panicf("cannot handle event of type %s", reflect.TypeOf(evt))
	}

	c.Unlock()

	ctx.Pos = akita.HookPosAfterEvent
	c.InvokeHook(&ctx)

	return nil
}

func (c *PCIeSwitchConnection) tick(now akita.VTimeInSec) {
	c.needTick = false

	c.doDeliver(now)
	c.scheduleTransfer(now)

	if c.needTick {
		c.ticker.TickLater(now)
	}
}

func (c *PCIeSwitchConnection) doDeliver(now akita.VTimeInSec) {
	for _, buf := range c.dstBuffers {
		dst := c.dstBufferToPortMap[buf]

		if c.dstBusy[dst] {
			continue
		}

		if len(buf.Buf) == 0 {
			continue
		}

		msg := buf.Buf[0]
		msg.Meta().RecvTime = now
		err := dst.Recv(msg)
		if err == nil {
			buf.Buf = buf.Buf[1:]
			c.needTick = true

			ctx := akita.HookCtx{
				Domain: c,
				Now:    msg.Meta().SendTime,
				Pos:    akita.HookPosConnDeliver,
				Item:   msg,
			}
			c.InvokeHook(&ctx)

		} else {
			c.dstBusy[dst] = true
		}
		break
	}
}

func (c *PCIeSwitchConnection) scheduleTransfer(now akita.VTimeInSec) {
	if c.busy >= c.NumLanes {
		return
	}

	srcIndex := c.lastSelectedSrcBuf
	for i := 0; i < len(c.srcPools); i++ {
		srcIndex = (c.lastSelectedSrcBuf + i + 1) % len(c.srcPools)

		sent := c.trySendFromSrc(now, srcIndex)
		if sent {
			c.lastSelectedSrcBuf = srcIndex
			break
		}
	}
}

func (c *PCIeSwitchConnection) trySendFromSrc(
	now akita.VTimeInSec,
	srcIndex int,
) bool {
	bufpool := c.srcPools[srcIndex]
	LastIndex := bufpool.lastSelectedVC
	for i := 0; i < c.totalVC; i++ {
		vcIndex := (LastIndex + i + 1) % c.totalVC

		buf := bufpool.Pool[vcIndex]
		if buf == nil {
			log.Panic("not connected of virtual channel ", vcIndex)
		}

		if c.srcBufferBusy[buf] {
			continue
		}
		if len(buf.Buf) == 0 {
			continue
		}

		msg := buf.Buf[0]
		dst := msg.Meta().Dst
		dstBuf := c.dstPortToBufferMap[dst]
		if len(dstBuf.Buf)+c.dstNumArrivingMsgs[dst] >= c.DstBufferCapacity {
			continue
		}

		c.needTick = true
		cycles := ((msg.Meta().TrafficBytes - 1) / c.BytesPerCycle) + 1
		transferTime := c.Freq.NCyclesLater(cycles, now)
		transferEvent := NewTransferEvent(transferTime, c, msg, vcIndex)
		c.engine.Schedule(transferEvent)

		ctx := akita.HookCtx{
			Domain: c,
			Now:    msg.Meta().SendTime,
			Pos:    akita.HookPosConnStartTrans,
			Item:   msg,
		}
		c.InvokeHook(&ctx)

		c.busy++
		c.srcBufferBusy[buf] = true
		bufpool.lastSelectedVC = vcIndex
		return true
	}
	return false
}

func (c *PCIeSwitchConnection) doTransfer(evt *TransferEvent) {
	now := evt.Time()
	msg := evt.msg
	src := msg.Meta().Src
	dst := msg.Meta().Dst
	vc := evt.vc

	srcPool := c.srcPortToPoolMap[src]
	srcBuf := srcPool.Pool[vc]
	dstBuf := c.dstPortToBufferMap[dst]

	srcBuf.Buf = srcBuf.Buf[1:]
	dstBuf.Buf = append(dstBuf.Buf, msg)

	c.busy--
	c.srcBufferBusy[srcBuf] = false
	c.ticker.TickLater(now)

	//fmt.Printf("Tranfer %s -> %s\n",
	//	msg.Src().Comp.Name(), msg.Dst().Comp.Name())

	ctx := akita.HookCtx{
		Domain: c,
		Now:    msg.Meta().SendTime,
		Pos:    akita.HookPosConnDoneTrans,
		Item:   msg,
	}
	c.InvokeHook(&ctx)

	src.NotifyAvailable(evt.Time())
}

// Send asks the connection to send the message to the destination.
func (c *PCIeSwitchConnection) Send(msg akita.Msg) *akita.SendError {
	c.Lock()
	defer c.Unlock()

	srcPool := c.srcPortToPoolMap[msg.Meta().Src]
	vc := c.findVCViaTC(msg.Meta().TrafficClass)
	buf := srcPool.Pool[vc]

	if buf == nil {
		log.Panic("not connected")
	}

	if len(buf.Buf) >= c.SrcBufferCapacity {
		return akita.NewSendError()
	}

	//fmt.Printf("Start send %s -> %s\n",
	//	msg.Src().Comp.Name(), msg.Dst().Comp.Name())

	buf.enqueue(msg)
	c.ticker.TickLater(msg.Meta().SendTime)

	ctx := akita.HookCtx{
		Domain: c,
		Now:    msg.Meta().SendTime,
		Pos:    akita.HookPosConnStartSend,
		Item:   msg,
	}
	c.InvokeHook(&ctx)

	return nil
}

func (c *PCIeSwitchConnection) findVCViaTC(tc int) int {
	vc := tc % c.totalVC
	return vc
}

func (c *PCIeSwitchConnection) PlugIn(port akita.Port) {
	_, connected := c.srcPortToPoolMap[port]
	if connected {
		log.Panic("port already connected")
	}

	srcPool := new(MsgPool)
	for i := 0; i < c.totalVC; i++ {

		srcBuf := &MsgBuffer{
			Capacity: c.SrcBufferCapacity,
			vc:       i,
		}
		srcPool.Pool = append(srcPool.Pool, srcBuf)
		c.srcBufferBusy[srcBuf] = false
	}
	c.srcPortToPoolMap[port] = srcPool
	c.srcPools = append(c.srcPools, srcPool)

	dstBuf := &MsgBuffer{
		Capacity: c.DstBufferCapacity,
	}
	c.dstBuffers = append(c.dstBuffers, dstBuf)
	c.dstBufferToPortMap[dstBuf] = port
	c.dstPortToBufferMap[port] = dstBuf
	c.dstNumArrivingMsgs[port] = 0
	c.dstBusy[port] = false

	port.SetConnection(c)
}

func (PCIeSwitchConnection) Unplug(port akita.Port) {
	panic("implement me")
}

func (c *PCIeSwitchConnection) NotifyAvailable(now akita.VTimeInSec, port akita.Port) {
	c.dstBusy[port] = false
	c.ticker.TickLater(now)
}

func NewPCIeSwitchConnection(
	bytesPerCycle int,
	engine akita.Engine,
	freq akita.Freq,
) *PCIeSwitchConnection {
	conn := new(PCIeSwitchConnection)

	conn.BytesPerCycle = bytesPerCycle
	conn.SrcBufferCapacity = 1
	conn.DstBufferCapacity = 1
	conn.srcPortToPoolMap = make(map[akita.Port]*MsgPool)
	conn.srcBufferBusy = make(map[*MsgBuffer]bool)
	conn.dstBufferToPortMap = make(map[*MsgBuffer]akita.Port)
	conn.dstPortToBufferMap = make(map[akita.Port]*MsgBuffer)
	conn.dstNumArrivingMsgs = make(map[akita.Port]int)
	conn.dstBusy = make(map[akita.Port]bool)

	conn.engine = engine
	conn.Freq = freq
	conn.ticker = akita.NewTicker(conn, engine, conn.Freq)

	conn.NumLanes = 1

	conn.totalVC = 8
	return conn
}
