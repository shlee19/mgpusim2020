package tracing

import (
	"fmt"
	"reflect"

	"gitlab.com/akita/akita"
)

// NamedHookable represent something both have a name and can be hooked
type NamedHookable interface {
	akita.Named
	akita.Hookable
	InvokeHook(*akita.HookCtx)
}

// A list of hook poses for the hooks to apply to
var (
	HookPosTaskStart = &akita.HookPos{Name: "HookPosTaskStart"}
	HookPosTaskStep  = &akita.HookPos{Name: "HookPosTaskStep"}
	HookPosTaskEnd   = &akita.HookPos{Name: "HookPosTaskEnd"}
)

// StartTask notifies the hooks that hook to the domain about the start of a
// task.
func StartTask(
	id string,
	parentID string,
	now akita.VTimeInSec,
	domain NamedHookable,
	kind string,
	what string,
	detail interface{},
) {
	task := Task{
		ID:        id,
		ParentID:  parentID,
		StartTime: now,
		Kind:      kind,
		What:      what,
		Where:     domain.Name(),
		Detail:    detail,
	}
	ctx := akita.HookCtx{
		Now:    now,
		Domain: domain,
		Item:   task,
		Pos:    HookPosTaskStart,
	}
	domain.InvokeHook(&ctx)
}

// AddTaskStep marks that a milestone has been reached when processing a task.
func AddTaskStep(
	id string,
	now akita.VTimeInSec,
	domain NamedHookable,
	what string,
) {
	step := TaskStep{
		Time:  now,
		What:  what,
		Where: domain.Name(),
	}
	task := Task{
		ID:    id,
		Steps: []TaskStep{step},
	}
	ctx := akita.HookCtx{
		Now:    now,
		Domain: domain,
		Item:   task,
		Pos:    HookPosTaskStep,
	}
	domain.InvokeHook(&ctx)
}

// EndTask notifies the hooks about the end of a task.
func EndTask(
	id string,
	now akita.VTimeInSec,
	domain NamedHookable,
) {
	task := Task{
		ID:      id,
		EndTime: now,
	}
	ctx := akita.HookCtx{
		Now:    now,
		Domain: domain,
		Item:   task,
		Pos:    HookPosTaskEnd,
	}
	domain.InvokeHook(&ctx)
}

// ReqIDAtReceiver generates a standard ID for the request task at the
// request receiver.
func ReqIDAtReceiver(req akita.Req, domain NamedHookable) string {
	return fmt.Sprintf("%s@%s", req.GetID(), domain.Name())
}

// TraceReqInitiate generatse a new task. The new task has Type="req_out", What=[the
// type name of the request]. This function is to be called by the sender of
// the request.
func TraceReqInitiate(
	req akita.Req,
	now akita.VTimeInSec,
	domain NamedHookable,
	taskParentID string,
) {
	StartTask(
		req.GetID(),
		taskParentID,
		now,
		domain,
		"req_out",
		reflect.TypeOf(req).String(),
		req,
	)
}

// TraceReqReceive generates a new task for the request handling. The type of the
// task is always "req_in".
func TraceReqReceive(
	req akita.Req,
	now akita.VTimeInSec,
	domain NamedHookable,
) {
	StartTask(
		ReqIDAtReceiver(req, domain),
		req.GetID(),
		now,
		domain,
		"req_in",
		reflect.TypeOf(req).String(),
		req,
	)
}

// TraceReqComplete terminates the request handling task.
func TraceReqComplete(
	req akita.Req,
	now akita.VTimeInSec,
	domain NamedHookable,
) {
	EndTask(ReqIDAtReceiver(req, domain), now, domain)
}

// TraceReqFinalize terminates the request task. This function should be called
// when the sender receives the response.
func TraceReqFinalize(
	req akita.Req,
	now akita.VTimeInSec,
	domain NamedHookable,
) {
	EndTask(req.GetID(), now, domain)
}
