module gitlab.com/akita/akita

require (
	github.com/golang/mock v1.2.0
	github.com/onsi/ginkgo v1.7.0
	github.com/onsi/gomega v1.4.3
	github.com/rs/xid v1.2.1
)

go 1.13
