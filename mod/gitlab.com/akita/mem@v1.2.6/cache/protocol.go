package cache

import "gitlab.com/akita/akita"

// FlushReq is the request send to a cache unit to request it to flush all
// the cache lines.
type FlushReq struct {
	*akita.ReqBase
	DiscardInflight    bool
	PauseAfterFlushing bool
}

// NewFlushReq creates a new flush request
func NewFlushReq(time akita.VTimeInSec, src, dst akita.Port) *FlushReq {
	reqBase := akita.NewReqBase()
	reqBase.SetSendTime(time)
	reqBase.SetSrc(src)
	reqBase.SetDst(dst)
	return &FlushReq{
		ReqBase: reqBase,
	}
}

// FlushRsp is the respond sent from the a cache unit for finishing a cache
// flush
type FlushRsp struct {
	*akita.ReqBase
	RspTo string
}

// NewFlushRsp creates a new flush response
func NewFlushRsp(
	time akita.VTimeInSec,
	src, dst akita.Port,
	rspTo string,
) *FlushRsp {
	reqBase := akita.NewReqBase()
	reqBase.SetSendTime(time)
	reqBase.SetSrc(src)
	reqBase.SetDst(dst)
	return &FlushRsp{
		ReqBase: reqBase,
		RspTo:   rspTo,
	}
}

// ResetReq is the request send to a cache unit to request it to reset and pause the cache
type ResetReq struct {
	*akita.ReqBase
}

// NewResetReq creates a new reset request
func NewResetReq(time akita.VTimeInSec, src, dst akita.Port) *ResetReq {
	reqBase := akita.NewReqBase()
	reqBase.SetSendTime(time)
	reqBase.SetSrc(src)
	reqBase.SetDst(dst)
	return &ResetReq{
		ReqBase: reqBase,
	}
}

// ResetRsp is the respond sent from the a cache unit for finishing a cache reset
type ResetRsp struct {
	*akita.ReqBase
	RspTo string
}

// NewResetRsp creates a new Reset response
func NewResetRsp(
	time akita.VTimeInSec,
	src, dst akita.Port,
	rspTo string,
) *ResetRsp {
	reqBase := akita.NewReqBase()
	reqBase.SetSendTime(time)
	reqBase.SetSrc(src)
	reqBase.SetDst(dst)
	return &ResetRsp{
		ReqBase: reqBase,
		RspTo:   rspTo,
	}
}

// ResetReq is the request send to a cache unit to request it unpause the cache
type RestartReq struct {
	*akita.ReqBase
}

// NewRestartReq creates a new restart request
func NewRestartReq(time akita.VTimeInSec, src, dst akita.Port) *RestartReq {
	reqBase := akita.NewReqBase()
	reqBase.SetSendTime(time)
	reqBase.SetSrc(src)
	reqBase.SetDst(dst)
	return &RestartReq{
		ReqBase: reqBase,
	}
}

//RestartRsp is the request sent from cache unit for finishing a restart
type RestartRsp struct {
	*akita.ReqBase
}

//NewRestartRsp creates a new restart rsp
func NewRestartRsp(time akita.VTimeInSec, src, dst akita.Port) *RestartRsp {
	reqBase := akita.NewReqBase()
	reqBase.SetSendTime(time)
	reqBase.SetSrc(src)
	reqBase.SetDst(dst)
	return &RestartRsp{
		ReqBase: reqBase,
	}
}
