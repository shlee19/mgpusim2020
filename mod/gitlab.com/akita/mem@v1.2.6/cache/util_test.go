package cache

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("GetCacheLineID", func() {
	It("should get cache line ID and offset", func() {
		id, offset := GetCacheLineID(0x104, 6)
		Expect(id).To(Equal(uint64(0x100)))
		Expect(offset).To(Equal(uint64(4)))
	})
})
