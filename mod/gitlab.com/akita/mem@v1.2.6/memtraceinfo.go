package mem

import "gitlab.com/akita/akita"

// TraceInfo stores the information to be potentially captured by memory trace
type TraceInfo struct {
	Req      akita.Req
	When     akita.VTimeInSec
	What     string
	Where    string
	Address  uint64
	ByteSize uint64
	Data     []byte
}
