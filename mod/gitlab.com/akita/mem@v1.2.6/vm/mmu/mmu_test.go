package mmu

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem/vm"
)

var _ = Describe("MMU", func() {

	var (
		mockCtrl      *gomock.Controller
		engine        *MockEngine
		toTop         *MockPort
		migrationPort *MockPort
		topSender     *MockBufferedSender
		mmu           *MMUImpl
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		engine = NewMockEngine(mockCtrl)
		toTop = NewMockPort(mockCtrl)
		migrationPort = NewMockPort(mockCtrl)
		topSender = NewMockBufferedSender(mockCtrl)

		builder := MakeBuilder().WithEngine(engine)
		mmu = builder.Build("mmu")
		mmu.ToTop = toTop
		mmu.topSender = topSender
		mmu.MigrationPort = migrationPort
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should create page", func() {
		page := &vm.Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
			GPUID:    1,
		}
		mmu.CreatePage(page)

		Expect(mmu.PageTables).To(HaveLen(1))
		Expect(mmu.PageTables[1].FindPage(0x1000)).To(BeIdenticalTo(page))
	})

	It("should translate", func() {
		page := &vm.Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
			GPUID:    1,
		}
		mmu.CreatePage(page)

		pAddr, retPage := mmu.Translate(1, 0x1020)

		Expect(pAddr).To(Equal(uint64(0x20)))
		Expect(retPage).To(BeIdenticalTo(page))
	})

	It("should not find non-existing page", func() {
		page := &vm.Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
			GPUID:    1,
		}
		mmu.CreatePage(page)

		_, retPage := mmu.Translate(1, 0x2000)

		Expect(retPage).To(BeNil())
	})

	Context("parse top", func() {
		It("should process translation request", func() {
			translationReq := vm.NewTranslationReq(
				10, nil, mmu.ToTop, 1, 0x100000100, 0)
			toTop.EXPECT().Retrieve(akita.VTimeInSec(10)).Return(*translationReq)

			mmu.parseFromTop(10)

			Expect(mmu.numRequestsInFlight == 1)
		})

		It("should set MMU to max in flight requests", func() {
			mmu.numRequestsInFlight = 15
			translationReq := vm.NewTranslationReq(
				10, nil, mmu.ToTop, 1, 0x100000100, 0)
			toTop.EXPECT().Retrieve(akita.VTimeInSec(10)).Return(*translationReq)

			mmu.parseFromTop(10)

			Expect(mmu.numRequestsInFlight == 16)
		})

		It("should stall parse from top if MMU is servicing max requests", func() {
			mmu.numRequestsInFlight = 16

			madeProgress := mmu.parseFromTop(10)

			Expect(madeProgress).To(BeFalse())
		})
	})

	Context("walk page table", func() {
		It("should reduce translation cycles", func() {
			req := vm.NewTranslationReq(10, nil, toTop, 1, 0x1020, 0)
			walking := transaction{req: *req, cycleLeft: 10}
			mmu.walkingTranslations = append(mmu.walkingTranslations, walking)

			madeProgress := mmu.walkPageTable(11)

			Expect(mmu.walkingTranslations[0].cycleLeft).To(Equal(9))
			Expect(madeProgress).To(BeTrue())
		})

		It("should do nothing if page is being migrated", func() {
			page := &vm.Page{
				PID:         1,
				VAddr:       0x1000,
				PAddr:       0x0,
				PageSize:    4096,
				Valid:       true,
				IsMigrating: true,
			}
			mmu.CreatePage(page)
			req := vm.NewTranslationReq(10, nil, toTop, 1, 0x1020, 0)
			walking := transaction{req: *req, cycleLeft: 0}
			mmu.walkingTranslations = append(mmu.walkingTranslations, walking)
			mmu.numRequestsInFlight = 1

			madeProgress := mmu.walkPageTable(11)

			Expect(madeProgress).To(BeFalse())
			Expect(mmu.numRequestsInFlight).To(Equal(1))
		})

		Context("walk hit", func() {
			It("should send rsp to top if hit", func() {
				page := &vm.Page{
					PID:      1,
					VAddr:    0x1000,
					PAddr:    0x0,
					PageSize: 4096,
					Valid:    true,
				}
				mmu.CreatePage(page)
				req := vm.NewTranslationReq(
					10, nil, mmu.ToTop, 1, 0x1000, 0)
				walking := transaction{req: *req, cycleLeft: 0}
				mmu.walkingTranslations = append(mmu.walkingTranslations, walking)
				mmu.numRequestsInFlight = 1

				topSender.EXPECT().CanSend(1).Return(true)
				topSender.EXPECT().Send(gomock.Any())

				madeProgress := mmu.walkPageTable(11)

				Expect(madeProgress).To(BeTrue())
				Expect(mmu.numRequestsInFlight).To(Equal(0))
				Expect(mmu.walkingTranslations).To(HaveLen(0))
			})

			It("should stall if cannot send to top", func() {
				page := &vm.Page{
					PID:      1,
					VAddr:    0x1000,
					PAddr:    0x0,
					PageSize: 4096,
					Valid:    true,
				}
				mmu.CreatePage(page)
				req := vm.NewTranslationReq(
					10, nil, mmu.ToTop, 1, 0x1000, 0)
				walking := transaction{req: *req, cycleLeft: 0}
				mmu.walkingTranslations =
					append(mmu.walkingTranslations, walking)
				mmu.numRequestsInFlight = 1

				topSender.EXPECT().CanSend(1).Return(false)

				madeProgress := mmu.walkPageTable(11)

				Expect(madeProgress).To(BeFalse())
				Expect(mmu.numRequestsInFlight).To(Equal(1))
			})
		})

		Context("migration", func() {
			var (
				page    *vm.Page
				req     *vm.TranslationReq
				walking transaction
			)

			BeforeEach(func() {
				page = &vm.Page{
					PID:      1,
					VAddr:    0x1000,
					PAddr:    0x0,
					PageSize: 4096,
					Valid:    true,
					GPUID:    2,
					Unified:  true,
				}
				mmu.CreatePage(page)
				req = vm.NewTranslationReq(
					10, nil, mmu.ToTop, 1, 0x1000, 1)
				walking = transaction{req: *req, cycleLeft: 0}
				mmu.walkingTranslations = append(
					mmu.walkingTranslations, walking)
			})

			It("should stall if the MMU is doing another migration", func() {
				mmu.isDoingMigration = true

				madeProgress := mmu.walkPageTable(11)

				Expect(madeProgress).To(BeFalse())
			})

			It("should stall if send failed", func() {
				migrationPort.EXPECT().
					Send(gomock.Any()).
					Return(akita.NewSendError())

				madeProgress := mmu.walkPageTable(11)

				Expect(madeProgress).To(BeFalse())
				Expect(mmu.walkingTranslations).To(HaveLen(1))
			})

			It("should send to migrator if need migration", func() {
				migrationPort.EXPECT().Send(gomock.Any())

				madeProgress := mmu.walkPageTable(11)

				Expect(madeProgress).To(BeTrue())
				Expect(mmu.walkingTranslations).To(HaveLen(0))
				Expect(mmu.currentMigration.migration).NotTo(BeNil())
				Expect(mmu.isDoingMigration).To(BeTrue())
				Expect(page.IsMigrating).To(BeTrue())
			})
		})
	})

	Context("should return migrated page information", func() {
		var (
			page          *vm.Page
			req           *vm.TranslationReq
			migrating     transaction
			migrationDone *vm.PageMigrationRspFromDriver
		)

		BeforeEach(func() {
			page = &vm.Page{
				PID:         1,
				VAddr:       0x1000,
				PAddr:       0x0,
				PageSize:    4096,
				Valid:       true,
				GPUID:       1,
				Unified:     true,
				IsMigrating: true,
			}
			mmu.CreatePage(page)
			req = vm.NewTranslationReq(
				10, nil, mmu.ToTop, 1, 0x1000, 1)
			migrating = transaction{req: *req, cycleLeft: 0}
			mmu.currentMigration = migrating
			mmu.numRequestsInFlight = 1
			migrationDone = vm.NewPageMigrationRspFromDriver(0, nil, nil)
		})

		It("should do nothing if no respond", func() {
			migrationPort.EXPECT().Peek().Return(nil)

			madeProgress := mmu.processMigrationReturn(10)

			Expect(madeProgress).To(BeFalse())
		})

		It("should stall if send to top failed", func() {
			migrationPort.EXPECT().Peek().Return(migrationDone)
			topSender.EXPECT().CanSend(1).Return(false)

			madeProgress := mmu.processMigrationReturn(10)

			Expect(madeProgress).To(BeFalse())
			Expect(mmu.numRequestsInFlight).To(Equal(1))
			Expect(mmu.isDoingMigration).To(BeFalse())

		})

		It("should send rsp to top", func() {
			migrationPort.EXPECT().Peek().Return(migrationDone)
			topSender.EXPECT().CanSend(1).Return(true)
			topSender.EXPECT().Send(gomock.Any()).
				Do(func(rsp vm.TranslationReadyRsp) {
					Expect(rsp.Page).To(Equal(page))
				})
			migrationPort.EXPECT().Retrieve(gomock.Any())

			madeProgress := mmu.processMigrationReturn(10)

			Expect(madeProgress).To(BeTrue())
			Expect(page.IsMigrating).To(BeFalse())
			Expect(mmu.numRequestsInFlight).To(Equal(0))
			Expect(mmu.isDoingMigration).To(BeFalse())
		})

	})

	It("should invalidate a page. Subsequent transactions should return invalid page", func() {
		page := &vm.Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x0,
			PageSize: 4096,
			Valid:    true,
			GPUID:    1,
		}
		mmu.CreatePage(page)

		pAddr, retPage := mmu.Translate(1, 0x1020)

		Expect(pAddr).To(Equal(uint64(0x20)))
		Expect(retPage).To(BeIdenticalTo(page))
	})
})

var _ = Describe("MMU Integration", func() {
	var (
		mockCtrl   *gomock.Controller
		engine     akita.Engine
		mmu        *MMUImpl
		agent      *MockPort
		connection akita.Connection
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		engine = akita.NewSerialEngine()

		builder := MakeBuilder().WithEngine(engine)
		mmu = builder.Build("mmu")
		agent = NewMockPort(mockCtrl)
		connection = akita.NewDirectConnection(engine)

		agent.EXPECT().SetConnection(connection)
		connection.PlugIn(agent)
		connection.PlugIn(mmu.ToTop)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should lookup", func() {
		page := &vm.Page{
			PID:      1,
			VAddr:    0x1000,
			PAddr:    0x2000,
			PageSize: 4096,
			Valid:    true,
			GPUID:    1,
		}
		mmu.CreatePage(page)

		req := vm.NewTranslationReq(10, agent, mmu.ToTop, 1, 0x1000, 1)
		req.SetRecvTime(10)
		mmu.ToTop.Recv(*req)

		agent.EXPECT().Recv(gomock.Any()).
			Do(func(rsp vm.TranslationReadyRsp) {
				Expect(rsp.Page).To(Equal(page))
				Expect(rsp.RespondTo).To(Equal(req.ID))
			})

		engine.Run()
	})
})
