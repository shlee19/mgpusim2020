package addresstranslator

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/mem/vm"
	"gitlab.com/akita/util/akitaext"
	"gitlab.com/akita/util/tracing"
)

type transaction struct {
	incomingReqs    []mem.AccessReq
	translationReq  vm.TranslationReq
	translationRsp  vm.TranslationReadyRsp
	translationDone bool
}

type reqToBottom struct {
	reqFromTop  mem.AccessReq
	reqToBottom mem.AccessReq
}

// AddressTranslator is a component that forwards the read/write requests with
// the address translated from virtual to physical.
type AddressTranslator struct {
	*akitaext.TickingComponent

	TopPort         akita.Port
	BottomPort      akita.Port
	TranslationPort akita.Port

	lowModuleFinder     cache.LowModuleFinder
	translationProvider akita.Port
	log2PageSize        uint64
	gpuID               uint64
	numReqPerCycle      int

	transactions        []*transaction
	inflightReqToBottom []reqToBottom
}

// Tick updates state at each cycle
func (t *AddressTranslator) Tick(now akita.VTimeInSec) bool {
	madeProgress := false

	for i := 0; i < t.numReqPerCycle; i++ {
		madeProgress = t.respond(now) || madeProgress
	}

	for i := 0; i < t.numReqPerCycle; i++ {
		madeProgress = t.forward(now) || madeProgress
	}

	for i := 0; i < t.numReqPerCycle; i++ {
		madeProgress = t.parseTranslation(now) || madeProgress
	}

	for i := 0; i < t.numReqPerCycle; i++ {
		madeProgress = t.translate(now) || madeProgress
	}

	return madeProgress
}

func (t *AddressTranslator) translate(now akita.VTimeInSec) bool {
	item := t.TopPort.Peek()
	if item == nil {
		return false
	}

	req := item.(mem.AccessReq)
	vAddr := req.GetAddress()
	vPageID := t.addrToPageID(vAddr)

	transReq := vm.NewTranslationReq(now,
		t.TranslationPort, t.translationProvider,
		req.GetPID(), vPageID, t.gpuID)
	err := t.TranslationPort.Send(*transReq)
	if err != nil {
		return false
	}

	translation := &transaction{
		incomingReqs:   []mem.AccessReq{req},
		translationReq: *transReq,
	}
	t.transactions = append(t.transactions, translation)

	tracing.TraceReqReceive(req, now, t)
	tracing.TraceReqInitiate(transReq, now, t, tracing.ReqIDAtReceiver(req, t))

	t.TopPort.Retrieve(now)
	return true
}

func (t *AddressTranslator) parseTranslation(now akita.VTimeInSec) bool {
	rsp := t.TranslationPort.Retrieve(now)
	if rsp == nil {
		return false
	}

	transRsp := rsp.(vm.TranslationReadyRsp)
	translation := t.findTranslationByReqID(transRsp.RespondTo)
	translation.translationRsp = transRsp
	translation.translationDone = true

	tracing.TraceReqFinalize(translation.translationReq, now, t)

	return true
}

func (t *AddressTranslator) forward(now akita.VTimeInSec) bool {
	if len(t.transactions) == 0 || !t.transactions[0].translationDone {
		return false
	}

	translation := t.transactions[0]
	reqFromTop := translation.incomingReqs[0]
	translatedReq := t.createTranslatedReq(
		reqFromTop,
		*translation.translationRsp.Page)
	translatedReq.SetSendTime(now)
	err := t.BottomPort.Send(translatedReq)
	if err != nil {
		return false
	}

	t.inflightReqToBottom = append(t.inflightReqToBottom,
		reqToBottom{
			reqFromTop:  reqFromTop,
			reqToBottom: translatedReq,
		})
	translation.incomingReqs = translation.incomingReqs[1:]
	if len(translation.incomingReqs) == 0 {
		t.removeExistingTranslation(translation)
	}

	tracing.TraceReqInitiate(translatedReq, now, t,
		tracing.ReqIDAtReceiver(reqFromTop, t))

	return true
}

func (t *AddressTranslator) respond(now akita.VTimeInSec) bool {
	rsp := t.BottomPort.Peek()
	if rsp == nil {
		return false
	}

	var reqFromTop mem.AccessReq
	var reqToBottomCombo reqToBottom
	var rspToTop mem.MemRsp
	switch rsp := rsp.(type) {
	case *mem.DataReadyRsp:
		reqToBottomCombo = t.findReqToBottomByID(rsp.RespondTo)
		reqFromTop = reqToBottomCombo.reqFromTop
		drToTop := mem.NewDataReadyRsp(now,
			t.TopPort, reqFromTop.Src(),
			reqFromTop.GetID())
		drToTop.Data = rsp.Data
		rspToTop = drToTop
	case *mem.DoneRsp:
		reqToBottomCombo = t.findReqToBottomByID(rsp.RespondTo)
		reqFromTop = reqToBottomCombo.reqFromTop
		rspToTop = mem.NewDoneRsp(now,
			t.TopPort, reqFromTop.Src(), reqFromTop.GetID())
	default:
		log.Panicf("cannot handle respond of type %s", reflect.TypeOf(rsp))
	}

	err := t.TopPort.Send(rspToTop)
	if err != nil {
		return false
	}

	t.removeReqToBottomByID(rsp.(mem.MemRsp).GetRespondTo())
	t.BottomPort.Retrieve(now)

	tracing.TraceReqFinalize(reqToBottomCombo.reqToBottom, now, t)
	tracing.TraceReqComplete(reqToBottomCombo.reqFromTop, now, t)

	return true
}

func (t *AddressTranslator) createTranslatedReq(
	req mem.AccessReq,
	page vm.Page,
) mem.AccessReq {
	switch req := req.(type) {
	case *mem.ReadReq:
		return t.createTranslatedReadReq(req, page)
	case *mem.WriteReq:
		return t.createTranslatedWriteReq(req, page)
	default:
		log.Panicf("cannot translate request of type %s", reflect.TypeOf(req))
		return nil
	}
}

func (t *AddressTranslator) createTranslatedReadReq(
	req *mem.ReadReq,
	page vm.Page,
) *mem.ReadReq {
	offset := req.Address % (1 << t.log2PageSize)
	addr := page.PAddr + offset
	clone := mem.NewReadReq(0,
		t.BottomPort,
		t.lowModuleFinder.Find(addr),
		addr,
		req.MemByteSize)
	clone.PID = 0
	clone.IsLastInWave = req.IsLastInWave
	return clone
}

func (t *AddressTranslator) createTranslatedWriteReq(
	req *mem.WriteReq,
	page vm.Page,
) *mem.WriteReq {
	offset := req.Address % (1 << t.log2PageSize)
	addr := page.PAddr + offset
	clone := mem.NewWriteReq(0,
		t.BottomPort,
		t.lowModuleFinder.Find(addr),
		addr)
	clone.Data = req.Data
	clone.DirtyMask = req.DirtyMask
	clone.PID = 0
	clone.IsLastInWave = req.IsLastInWave
	return clone
}

func (t *AddressTranslator) addrToPageID(addr uint64) uint64 {
	return (addr >> t.log2PageSize) << t.log2PageSize
}

func (t *AddressTranslator) findTranslationByReqID(id string) *transaction {
	for _, t := range t.transactions {
		if t.translationReq.ID == id {
			return t
		}
	}
	panic("translation not found")
}

func (t *AddressTranslator) removeExistingTranslation(trans *transaction) {
	for i, tr := range t.transactions {
		if tr == trans {
			t.transactions = append(t.transactions[:i], t.transactions[i+1:]...)
			return
		}
	}
	panic("translation not found")
}

func (t *AddressTranslator) findReqToBottomByID(id string) reqToBottom {
	for _, r := range t.inflightReqToBottom {
		if r.reqToBottom.GetID() == id {
			return r
		}
	}
	panic("req to bottom not found")
}

func (t *AddressTranslator) removeReqToBottomByID(id string) {
	for i, r := range t.inflightReqToBottom {
		if r.reqToBottom.GetID() == id {
			t.inflightReqToBottom = append(
				t.inflightReqToBottom[:i],
				t.inflightReqToBottom[i+1:]...)
			return
		}
	}
	panic("req to bottom not found")
}
