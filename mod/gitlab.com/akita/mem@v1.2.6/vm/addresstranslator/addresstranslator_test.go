package addresstranslator

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/vm"
	"gitlab.com/akita/util/ca"
)

var _ = Describe("Address Translator", func() {
	var (
		mockCtrl        *gomock.Controller
		topPort         *MockPort
		bottomPort      *MockPort
		translationPort *MockPort
		lowModuleFinder *MockLowModuleFinder

		t *AddressTranslator
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		topPort = NewMockPort(mockCtrl)
		bottomPort = NewMockPort(mockCtrl)
		translationPort = NewMockPort(mockCtrl)
		lowModuleFinder = NewMockLowModuleFinder(mockCtrl)

		builder := MakeBuilder().
			WithLog2PageSize(12).
			WithFreq(1).
			WithLowModuleFinder(lowModuleFinder)
		t = builder.Build("address_translator")
		t.log2PageSize = 12
		t.TopPort = topPort
		t.BottomPort = bottomPort
		t.TranslationPort = translationPort
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	Context("translate stage", func() {
		var (
			req *mem.ReadReq
		)

		BeforeEach(func() {
			req = mem.NewReadReq(8, nil, nil, 0x100, 4)
			req.PID = 1
			req.IsLastInWave = true
		})

		It("should do nothing if there is no request", func() {
			topPort.EXPECT().Peek().Return(nil)
			madeProgress := t.translate(10)
			Expect(madeProgress).To(BeFalse())
		})

		It("should send translation", func() {
			var transReq vm.TranslationReq
			translation := &transaction{
				translationReq: *vm.NewTranslationReq(
					6, nil, nil, 1, 0x1000, 1),
			}
			t.transactions = append(t.transactions, translation)
			req.Address = 0x1040

			topPort.EXPECT().Peek().Return(req)
			topPort.EXPECT().Retrieve(gomock.Any())
			translationPort.EXPECT().Send(gomock.Any()).
				DoAndReturn(func(req vm.TranslationReq) *akita.SendError {
					transReq = req
					return nil
				})

			needTick := t.translate(10)

			Expect(needTick).To(BeTrue())
			Expect(translation.incomingReqs).NotTo(ContainElement(req))
			Expect(t.transactions).To(HaveLen(2))
			Expect(t.transactions[1].translationReq).
				To(BeEquivalentTo(transReq))
		})

		It("should stall if cannot send for translation", func() {
			topPort.EXPECT().Peek().Return(req)
			translationPort.EXPECT().
				Send(gomock.Any()).
				Return(&akita.SendError{})

			needTick := t.translate(10)

			Expect(needTick).To(BeFalse())
			Expect(t.transactions).To(HaveLen(0))
		})
	})

	Context("parse translation", func() {
		It("should do nothing if there is no translation return", func() {
			translationPort.EXPECT().Retrieve(gomock.Any()).Return(nil)
			needTick := t.parseTranslation(10)
			Expect(needTick).To(BeFalse())
		})

		It("should attach translation return to translation", func() {
			trans1 := &transaction{
				translationReq: *vm.NewTranslationReq(0, nil, nil, 1, 0x100, 1),
			}
			trans2 := &transaction{
				translationReq: *vm.NewTranslationReq(0, nil, nil, 1, 0x100, 1),
			}
			t.transactions = append(t.transactions, trans1, trans2)
			rsp := vm.NewTranslationReadyRsp(0,
				nil, nil, trans2.translationReq.ID)

			translationPort.EXPECT().Retrieve(gomock.Any()).Return(*rsp)

			madeProgress := t.parseTranslation(10)

			Expect(madeProgress).To(BeTrue())
			Expect(trans2.translationRsp).To(Equal(*rsp))
			Expect(trans2.translationDone).To(BeTrue())
		})
	})

	Context("forwarding", func() {
		var (
			trans1, trans2 *transaction
		)

		BeforeEach(func() {
			trans1 = &transaction{
				translationReq: *vm.NewTranslationReq(0, nil, nil, 1, 0x100, 1),
			}
			trans2 = &transaction{
				translationReq: *vm.NewTranslationReq(0, nil, nil, 1, 0x100, 1),
			}
			t.transactions = append(t.transactions, trans1, trans2)
		})

		It("should do nothing if first translation is not ready", func() {
			madeProgress := t.forward(1)

			Expect(madeProgress).To(BeFalse())
		})

		It("should forward read request", func() {
			req := mem.NewReadReq(6, nil, nil, 0x10040, 4)
			translateReady := vm.NewTranslationReadyRsp(
				8, nil, nil, "")
			translateReady.Page = &vm.Page{
				PID: 1, VAddr: 0x10000, PAddr: 0x20000}

			trans1.incomingReqs = []mem.AccessReq{req}
			trans1.translationRsp = *translateReady
			trans1.translationDone = true

			lowModuleFinder.EXPECT().Find(uint64(0x20040))
			bottomPort.EXPECT().Send(gomock.Any()).
				Do(func(read *mem.ReadReq) {
					Expect(read).NotTo(BeIdenticalTo(req))
					Expect(read.SendTime()).To(Equal(akita.VTimeInSec(10)))
					Expect(read.PID).To(Equal(ca.PID(0)))
					Expect(read.Address).To(Equal(uint64(0x20040)))
					Expect(read.MemByteSize).To(Equal(uint64(4)))
					Expect(read.Src()).To(BeIdenticalTo(bottomPort))
				}).
				Return(nil)

			madeProgress := t.forward(10)

			Expect(madeProgress).To(BeTrue())
			Expect(t.transactions).NotTo(ContainElement(trans1))
			Expect(t.inflightReqToBottom).To(HaveLen(1))
		})

		It("should stall if send failed", func() {
			req := mem.NewReadReq(6, nil, nil, 0x10040, 4)
			translateReady := vm.NewTranslationReadyRsp(
				8, nil, nil, "")
			translateReady.Page = &vm.Page{
				PID: 1, VAddr: 0x10000, PAddr: 0x20000}

			trans1.incomingReqs = []mem.AccessReq{req}
			trans1.translationRsp = *translateReady
			trans1.translationDone = true

			lowModuleFinder.EXPECT().Find(uint64(0x20040))
			bottomPort.EXPECT().Send(gomock.Any()).
				Do(func(read *mem.ReadReq) {
					Expect(read).NotTo(BeIdenticalTo(req))
					Expect(read.PID).To(Equal(ca.PID(0)))
					Expect(read.Address).To(Equal(uint64(0x20040)))
					Expect(read.MemByteSize).To(Equal(uint64(4)))
					Expect(read.Src()).To(BeIdenticalTo(bottomPort))
				}).
				Return(&akita.SendError{})

			needTick := t.forward(10)

			Expect(needTick).To(BeFalse())
			Expect(t.transactions).To(ContainElement(trans1))
			Expect(t.inflightReqToBottom).To(HaveLen(0))
		})

		It("should forward write request", func() {
			data := []byte{1, 2, 3, 4}
			dirty := []bool{false, true, false, true}
			write := mem.NewWriteReq(6, nil, nil, 0x10040)
			write.Data = data
			write.DirtyMask = dirty
			translateReady := vm.NewTranslationReadyRsp(
				8, nil, nil, "")
			translateReady.Page = &vm.Page{
				PID: 1, VAddr: 0x10000, PAddr: 0x20000}
			trans1.incomingReqs = []mem.AccessReq{write}
			trans1.translationRsp = *translateReady
			trans1.translationDone = true

			lowModuleFinder.EXPECT().Find(uint64(0x20040))
			bottomPort.EXPECT().Send(gomock.Any()).
				Do(func(req *mem.WriteReq) {
					Expect(req).NotTo(BeIdenticalTo(write))
					Expect(req.SendTime()).To(Equal(akita.VTimeInSec(10)))
					Expect(req.PID).To(Equal(ca.PID(0)))
					Expect(req.Address).To(Equal(uint64(0x20040)))
					Expect(req.Src()).To(BeIdenticalTo(bottomPort))
					Expect(req.Data).To(Equal(data))
					Expect(req.DirtyMask).To(Equal(dirty))
				}).
				Return(nil)

			needTick := t.forward(10)

			Expect(needTick).To(BeTrue())
			Expect(t.transactions).NotTo(ContainElement(trans1))
			Expect(t.inflightReqToBottom).To(HaveLen(1))
		})
	})

	Context("respond", func() {
		var (
			readFromTop   *mem.ReadReq
			writeFromTop  *mem.WriteReq
			readToBottom  *mem.ReadReq
			writeToBottom *mem.WriteReq
		)

		BeforeEach(func() {
			readFromTop = mem.NewReadReq(8, nil, nil, 0x10040, 4)
			readToBottom = mem.NewReadReq(9, nil, nil, 0x20040, 4)
			writeFromTop = mem.NewWriteReq(8, nil, nil, 0x10080)
			writeToBottom = mem.NewWriteReq(8, nil, nil, 0x20080)
			t.inflightReqToBottom = []reqToBottom{
				{reqFromTop: readFromTop, reqToBottom: readToBottom},
				{reqFromTop: writeFromTop, reqToBottom: writeToBottom},
			}

		})

		It("should do nothing if there is no response to process", func() {
			bottomPort.EXPECT().Peek().Return(nil)
			madeProgress := t.respond(10)
			Expect(madeProgress).To(BeFalse())
		})

		It("should respond data ready", func() {
			dataReady := mem.NewDataReadyRsp(10, nil, nil, readToBottom.ID)
			bottomPort.EXPECT().Peek().Return(dataReady)
			topPort.EXPECT().Send(gomock.Any()).
				Do(func(dr *mem.DataReadyRsp) {
					Expect(dr.RespondTo).To(Equal(readFromTop.ID))
					Expect(dr.Data).To(Equal(dataReady.Data))
				}).
				Return(nil)
			bottomPort.EXPECT().Retrieve(gomock.Any())

			madeProgress := t.respond(10)

			Expect(madeProgress).To(BeTrue())
			Expect(t.inflightReqToBottom).To(HaveLen(1))
		})

		It("should respond write done", func() {
			done := mem.NewDoneRsp(10, nil, nil, writeToBottom.ID)
			bottomPort.EXPECT().Peek().Return(done)
			topPort.EXPECT().Send(gomock.Any()).
				Do(func(done *mem.DoneRsp) {
					Expect(done.RespondTo).To(Equal(writeFromTop.ID))
				}).
				Return(nil)
			bottomPort.EXPECT().Retrieve(gomock.Any())

			madeProgress := t.respond(10)

			Expect(madeProgress).To(BeTrue())
			Expect(t.inflightReqToBottom).To(HaveLen(1))
		})

		It("should stall if TopPort is busy", func() {
			dataReady := mem.NewDataReadyRsp(10, nil, nil, readToBottom.ID)
			bottomPort.EXPECT().Peek().Return(dataReady)
			topPort.EXPECT().Send(gomock.Any()).
				Do(func(dr *mem.DataReadyRsp) {
					Expect(dr.RespondTo).To(Equal(readFromTop.ID))
					Expect(dr.Data).To(Equal(dataReady.Data))
				}).
				Return(&akita.SendError{})

			madeProgress := t.respond(10)

			Expect(madeProgress).To(BeFalse())
			Expect(t.inflightReqToBottom).To(HaveLen(2))
		})
	})
})
