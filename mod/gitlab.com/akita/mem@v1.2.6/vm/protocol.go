// Package vm provides the models for address translations
package vm

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/util/ca"
)

// A TranslationReq asks the receiver component to translate the request.
type TranslationReq struct {
	*akita.ReqBase

	VAddr uint64
	PID   ca.PID
	GPUID uint64
}

// NewTranslationReq creates a new translation req
func NewTranslationReq(
	time akita.VTimeInSec,
	src, dst akita.Port,
	pid ca.PID,
	vAddr uint64,
	GPUID uint64,
) *TranslationReq {
	reqBase := akita.NewReqBase()
	req := new(TranslationReq)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.VAddr = vAddr
	req.PID = pid
	req.GPUID = GPUID

	return req
}

// A TranslationReadyRsp is the respond for a TranslationReq. It carries the
// physical address.
type TranslationReadyRsp struct {
	*akita.ReqBase

	RespondTo string // The ID of the request it replies
	Page      *Page
}

// NewTranslationReadyRsp creates a new TranslationReadyRsp
func NewTranslationReadyRsp(
	time akita.VTimeInSec,
	src, dst akita.Port,
	respondTo string,
) *TranslationReadyRsp {
	reqBase := akita.NewReqBase()
	req := new(TranslationReadyRsp)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.RespondTo = respondTo

	return req
}

//A TLBFullInvalidate asks the receiver TLB to invalidate all its entries

type PTEInvalidationReq struct {
	*akita.ReqBase
	PID   ca.PID
	Vaddr []uint64
}

type InvalidationCompleteRsp struct {
	*akita.ReqBase

	RespondTo        string // The ID of the request it replies
	InvalidationDone bool
}

type TLBFullInvalidateReq struct {
	*akita.ReqBase
	PID ca.PID
}

//A TLBPartialInvalidate asks the receiver TLB to invalidate the list of particular entries.
// The list can consist anyhere from 0 to N entries
type TLBPartialInvalidateReq struct {
	*akita.ReqBase

	VAddr []uint64
	PID   ca.PID
}

type PageMigrationReqToDriver struct {
	*akita.ReqBase

	VAddr           uint64
	PhysicalAddress uint64
	PageSize        uint64
	PID             ca.PID
	RequestingGPU   uint64
}

type PageMigrationRspFromDriver struct {
	*akita.ReqBase

	Vaddr             uint64
	MigrationComplete bool
}

func NewInvalidationCompleteRsp(
	time akita.VTimeInSec,
	src, dst akita.Port,
	respondTo string,
) *InvalidationCompleteRsp {
	reqBase := akita.NewReqBase()
	req := new(InvalidationCompleteRsp)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.RespondTo = respondTo

	return req
}

func NewPTEInvalidationReq(
	time akita.VTimeInSec,
	src, dst akita.Port,
	pid ca.PID,
	vAddr []uint64,
) *PTEInvalidationReq {
	reqBase := akita.NewReqBase()
	req := new(PTEInvalidationReq)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	req.Vaddr = vAddr
	req.PID = pid

	return req
}

func NewPageMigrationReqToDriver(
	time akita.VTimeInSec,
	src, dst akita.Port,
) *PageMigrationReqToDriver {
	reqBase := akita.NewReqBase()
	req := new(PageMigrationReqToDriver)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	return req
}

func NewPageMigrationRspFromDriver(
	time akita.VTimeInSec,
	src, dst akita.Port,
) *PageMigrationRspFromDriver {
	reqBase := akita.NewReqBase()
	req := new(PageMigrationRspFromDriver)
	req.ReqBase = reqBase

	req.SetSendTime(time)
	req.SetSrc(src)
	req.SetDst(dst)

	return req
}
