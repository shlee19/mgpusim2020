package tlb

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem/vm"
	"gitlab.com/akita/util/ca"
)

var _ = Describe("TLB", func() {

	var (
		mockCtrl    *gomock.Controller
		engine      *MockEngine
		tlb         *TLB
		topPort     *MockPort
		bottomPort  *MockPort
		controlPort *MockPort
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		engine = NewMockEngine(mockCtrl)
		topPort = NewMockPort(mockCtrl)
		bottomPort = NewMockPort(mockCtrl)
		controlPort = NewMockPort(mockCtrl)

		tlb = MakeBuilder().WithEngine(engine).Build("tlb")
		tlb.TopPort = topPort
		tlb.BottomPort = bottomPort
		tlb.ControlPort = controlPort
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should do nothing if there is no req in TopPort", func() {
		topPort.EXPECT().Peek().Return(nil)

		madeProgress := tlb.lookup(10)

		Expect(madeProgress).To(BeFalse())
	})

	Context("hit", func() {
		var (
			page vm.Page
			req  vm.TranslationReq
		)

		BeforeEach(func() {
			page = vm.Page{
				PID:   1,
				VAddr: 0x100,
				PAddr: 0x200,
				Valid: true,
			}
			tlb.Sets[0].Blocks[2] = page

			req = *vm.NewTranslationReq(5, nil, nil, 1, 0x100, 1)
		})

		It("should respond to top", func() {
			topPort.EXPECT().Peek().Return(req)
			topPort.EXPECT().Retrieve(gomock.Any())
			topPort.EXPECT().Send(gomock.Any()).
				Do(func(rsp vm.TranslationReadyRsp) {
					Expect(rsp.Page).To(Equal(&page))
					Expect(rsp.RespondTo).To(Equal(req.ID))
				}).
				Return(nil)

			madeProgress := tlb.lookup(10)

			Expect(madeProgress).To(BeTrue())
			Expect(tlb.Sets[0].LRUQueue[31]).To(Equal(2))
		})

		It("should stall if cannot send to top", func() {
			topPort.EXPECT().Peek().Return(req)
			topPort.EXPECT().Send(gomock.Any()).
				Return(&akita.SendError{})

			madeProgress := tlb.lookup(10)

			Expect(madeProgress).To(BeFalse())
		})
	})

	Context("miss", func() {
		var (
			page vm.Page
			req  vm.TranslationReq
		)

		BeforeEach(func() {
			page = vm.Page{
				PID:   1,
				VAddr: 0x100,
				PAddr: 0x200,
				Valid: false,
			}
			tlb.Sets[0].Blocks[0] = page

			req = *vm.NewTranslationReq(5, nil, nil, 1, 0x100, 1)
		})

		It("should respond to bottom", func() {
			topPort.EXPECT().Peek().Return(req)
			topPort.EXPECT().Retrieve(gomock.Any())
			bottomPort.EXPECT().Send(gomock.Any()).
				Do(func(req vm.TranslationReq) {
					Expect(req.VAddr).To(Equal(uint64(0x100)))
					Expect(req.PID).To(Equal(ca.PID(1)))
					Expect(req.GPUID).To(Equal(uint64(1)))
				}).
				Return(nil)

			madeProgress := tlb.lookup(10)

			Expect(madeProgress).To(BeTrue())
			Expect(tlb.translating).To(HaveLen(1))
		})

		It("should stall is bottom is busy", func() {
			topPort.EXPECT().Peek().Return(req)
			bottomPort.EXPECT().Send(gomock.Any()).
				Return(&akita.SendError{})

			madeProgress := tlb.lookup(10)

			Expect(madeProgress).To(BeFalse())
		})
	})

	Context("parse bottom", func() {
		var (
			req         vm.TranslationReq
			fetchBottom vm.TranslationReq
			trans       transaction
			page        vm.Page
			rsp         vm.TranslationReadyRsp
		)

		BeforeEach(func() {
			req = *vm.NewTranslationReq(5, nil, nil, 1, 0x100, 1)
			fetchBottom = *vm.NewTranslationReq(5, nil, nil, 1, 0x100, 1)
			trans = transaction{
				req:         req,
				fetchBottom: fetchBottom,
			}
			tlb.translating = append(tlb.translating, trans)
			page = vm.Page{
				PID:   1,
				VAddr: 0x100,
				PAddr: 0x200,
				Valid: true,
			}
			rsp = *vm.NewTranslationReadyRsp(5, nil, nil, fetchBottom.ID)
			rsp.Page = &page
		})

		It("should do nothing if no return", func() {
			bottomPort.EXPECT().Peek().Return(nil)

			madeProgress := tlb.parseBottom(10)

			Expect(madeProgress).To(BeFalse())
		})

		It("should write from bottom", func() {
			bottomPort.EXPECT().Peek().Return(rsp)
			bottomPort.EXPECT().Retrieve(gomock.Any())
			topPort.EXPECT().Send(gomock.Any()).
				Do(func(rsp vm.TranslationReadyRsp) {
					Expect(rsp.Page).To(Equal(&page))
					Expect(rsp.RespondTo).To(Equal(req.ID))
				})

			madeProgress := tlb.parseBottom(10)

			Expect(madeProgress).To(BeTrue())
			Expect(tlb.translating).To(HaveLen(0))
			Expect(tlb.Sets[0].Blocks[0]).To(Equal(page))
			Expect(tlb.Sets[0].LRUQueue[31]).To(Equal(0))
		})

		It("should stall if cannot send to top", func() {
			bottomPort.EXPECT().Peek().Return(rsp)
			topPort.EXPECT().Send(gomock.Any()).
				Return(&akita.SendError{})

			madeProgress := tlb.parseBottom(10)

			Expect(madeProgress).To(BeFalse())
		})
	})

	Context("shootdown", func() {
		It("should do nothing if no req", func() {
			controlPort.EXPECT().Peek().Return(nil)

			madeProgress := tlb.performCtrlReq(10)

			Expect(madeProgress).To(BeFalse())
		})

		It("should stall if cannot send to control port", func() {
			req := vm.NewPTEInvalidationReq(10, nil, nil, 1, []uint64{0x1000})

			controlPort.EXPECT().Peek().Return(*req)
			controlPort.EXPECT().Send(gomock.Any()).Return(&akita.SendError{})

			madeProgress := tlb.performCtrlReq(10)

			Expect(madeProgress).To(BeFalse())
		})

		It("should shootdown", func() {
			tlb.Sets[0].Blocks[0].PID = 1
			tlb.Sets[0].Blocks[0].VAddr = 0x1000
			tlb.Sets[0].Blocks[0].Valid = true
			tlb.Sets[0].Blocks[1].PID = 1
			tlb.Sets[0].Blocks[1].VAddr = 0x2000
			tlb.Sets[0].Blocks[1].Valid = true
			tlb.Sets[0].Blocks[2].PID = 2
			tlb.Sets[0].Blocks[2].VAddr = 0x1000
			tlb.Sets[0].Blocks[2].Valid = true
			req := vm.NewPTEInvalidationReq(10, nil, nil, 1, []uint64{0x1000})

			controlPort.EXPECT().Peek().Return(*req)
			controlPort.EXPECT().Retrieve(gomock.Any())
			controlPort.EXPECT().Send(gomock.Any())

			madeProgress := tlb.performCtrlReq(10)

			Expect(madeProgress).To(BeTrue())
			Expect(tlb.Sets[0].Blocks[0].Valid).To(BeFalse())
			Expect(tlb.Sets[0].Blocks[1].Valid).To(BeTrue())
			Expect(tlb.Sets[0].Blocks[1].Valid).To(BeTrue())
		})
	})
})

var _ = Describe("TLB Integration", func() {
	var (
		mockCtrl   *gomock.Controller
		engine     akita.Engine
		tlb        *TLB
		lowModule  *MockPort
		agent      *MockPort
		connection akita.Connection
		page       vm.Page
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		engine = akita.NewSerialEngine()
		lowModule = NewMockPort(mockCtrl)
		agent = NewMockPort(mockCtrl)
		connection = akita.NewDirectConnection(engine)
		tlb = MakeBuilder().WithEngine(engine).Build("tlb")
		tlb.LowModule = lowModule

		agent.EXPECT().SetConnection(connection)
		lowModule.EXPECT().SetConnection(connection)
		connection.PlugIn(agent)
		connection.PlugIn(lowModule)
		connection.PlugIn(tlb.TopPort)
		connection.PlugIn(tlb.BottomPort)
		connection.PlugIn(tlb.ControlPort)

		page = vm.Page{
			PID:   1,
			VAddr: 0x1000,
			PAddr: 0x2000,
			Valid: true,
		}
		lowModule.EXPECT().Recv(gomock.Any()).
			Do(func(req vm.TranslationReq) {
				rsp := vm.NewTranslationReadyRsp(
					req.RecvTime()+1, lowModule, req.Src(), req.GetID())
				rsp.Page = &page
				connection.Send(*rsp)
			}).
			AnyTimes()
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should do tlb miss", func() {
		req := vm.NewTranslationReq(10, agent, tlb.TopPort, 1, 0x1000, 1)
		req.SetSendTime(10)
		tlb.TopPort.Recv(*req)

		agent.EXPECT().Recv(gomock.Any()).
			Do(func(rsp vm.TranslationReadyRsp) {
				Expect(rsp.Page).To(Equal(&page))
			})

		engine.Run()
	})

	It("should have faster hit than miss", func() {
		time1 := akita.VTimeInSec(10)
		req := vm.NewTranslationReq(time1, agent, tlb.TopPort, 1, 0x1000, 1)
		req.SetRecvTime(time1)
		tlb.TopPort.Recv(*req)

		agent.EXPECT().Recv(gomock.Any()).
			Do(func(rsp vm.TranslationReadyRsp) {
				Expect(rsp.Page).To(Equal(&page))
			})

		engine.Run()

		time2 := engine.CurrentTime()

		req.SetRecvTime(time2)
		tlb.TopPort.Recv(*req)

		agent.EXPECT().Recv(gomock.Any()).
			Do(func(rsp vm.TranslationReadyRsp) {
				Expect(rsp.Page).To(Equal(&page))
			})

		engine.Run()

		time3 := engine.CurrentTime()

		Expect(time3 - time2).To(BeNumerically("<", time2-time1))
	})

	It("should have miss after shootdown ", func() {
		time1 := akita.VTimeInSec(10)
		req := vm.NewTranslationReq(time1, agent, tlb.TopPort, 1, 0x1000, 1)
		req.SetRecvTime(time1)
		tlb.TopPort.Recv(*req)
		agent.EXPECT().Recv(gomock.Any()).
			Do(func(rsp vm.TranslationReadyRsp) {
				Expect(rsp.Page).To(Equal(&page))
			})
		engine.Run()

		time2 := engine.CurrentTime()
		shootdownReq := vm.NewPTEInvalidationReq(
			time2, agent, tlb.ControlPort, 1, []uint64{0x1000})
		shootdownReq.SetRecvTime(time2)
		tlb.ControlPort.Recv(*shootdownReq)
		agent.EXPECT().Recv(gomock.Any()).
			Do(func(rsp vm.InvalidationCompleteRsp) {
				Expect(rsp.RespondTo).To(Equal(shootdownReq.ID))
			})
		engine.Run()

		time3 := engine.CurrentTime()
		req.SetRecvTime(time3)
		tlb.TopPort.Recv(*req)
		agent.EXPECT().Recv(gomock.Any()).
			Do(func(rsp vm.TranslationReadyRsp) {
				Expect(rsp.Page).To(Equal(&page))
			})
		engine.Run()
		time4 := engine.CurrentTime()

		Expect(time4 - time3).To(BeNumerically("~", time2-time1))
	})

})
