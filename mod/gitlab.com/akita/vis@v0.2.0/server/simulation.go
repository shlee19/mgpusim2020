package main

import (
	"context"
	"github.com/mongodb/mongo-go-driver/bson"
	"time"
)

func GetSimulationTask() *Task {
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
	defer cancel()

	task := &Task{}
	res := collection.FindOne(ctx, bson.D{{"type", "Simulation"}})
	err := res.Decode(task)
	dieOnErr(err)

	return task
}

func GetSimulationStartEndTime() (float64, float64) {
	task := GetSimulationTask()
	return task.Start, task.End
}
