package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/mongodb/mongo-go-driver/mongo"
)

const usageMessage = "" +
	`
Usage of vis
	vis [flags] gcn3 dbName

Flags
	-http=addr: HTTP service address (e.g., ':6060')
	`

var (
	httpFlag = flag.String("http",
		"localhost:3001",
		"HTTP service address (e.g., ':6060')")
	model      string
	dbName     string
	collection *mongo.Collection
)

func main() {
	parseArgs()
	startServer()
}

func parseArgs() {
	flag.Usage = func() {
		fmt.Fprintln(os.Stderr, usageMessage)
		os.Exit(2)
	}

	flag.Parse()

	switch flag.NArg() {
	case 2:
		model = flag.Arg(0)
		dbName = flag.Arg(1)
	default:
		flag.Usage()
	}
}

func startServer() {
	connectToDB()
	startAPIServer()
}

func connectToDB() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, "mongodb://localhost:27017")
	if err != nil {
		log.Panic(err)
	}

	fmt.Printf("Open trace in db %s\n", dbName)

	collection = client.Database(dbName).Collection("trace")
}

func startAPIServer() {
	fs := http.FileServer(http.Dir("../" + model))

	http.Handle("/", fs)
	http.HandleFunc("/api/trace", httpTrace)
	http.HandleFunc("/api/ips", handleIPS)

	fmt.Printf("Listening %s\n", *httpFlag)
	err := http.ListenAndServe(*httpFlag, nil)
	dieOnErr(err)
}

func dieOnErr(err error) {
	if err != nil {
		log.Panic(err)
	}
}
