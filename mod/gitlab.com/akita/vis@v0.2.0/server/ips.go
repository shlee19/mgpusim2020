package main

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/mongodb/mongo-go-driver/bson"
)

type IPS struct {
	Start, End    float64
	BinDuration   float64
	StartCount    []float64
	CompleteCount []float64
}

func handleIPS(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
	defer cancel()

	startTime, endTime := GetSimulationStartEndTime()

	binCount := 100
	ips := IPS{
		Start:         startTime,
		End:           endTime,
		BinDuration:   (endTime - startTime) / float64(binCount),
		StartCount:    make([]float64, binCount+1),
		CompleteCount: make([]float64, binCount+1),
	}

	cursor, err := collection.Find(ctx, bson.D{{"type", "Inst"}})
	dieOnErr(err)
	for cursor.Next(ctx) {
		var task Task
		err := cursor.Decode(&task)
		dieOnErr(err)

		startBin := int((task.Start - startTime) / ips.BinDuration)
		ips.StartCount[startBin]++

		endBin := int((task.End - startTime) / ips.BinDuration)
		ips.CompleteCount[endBin]++
	}

	for i := 0; i < binCount+1; i++ {
		ips.StartCount[i] /= ips.BinDuration
		ips.CompleteCount[i] /= ips.BinDuration
	}

	var rsp []byte
	rsp, err = json.Marshal(ips)
	dieOnErr(err)
	_, err = w.Write(rsp)
	dieOnErr(err)
}
