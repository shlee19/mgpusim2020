package trace

// A Task tracks about how a goal is achieved in a simulator
type Task struct {
	ID           string      `json:"id,omitempty"`
	ParentID     string      `json:"parent_id,omitempty"`
	Type         string      `json:"type,omitempty"`
	What         string      `json:"what,omitempty"`
	Where        string      `json:"where,omitempty"`
	InitiateTime float64     `json:"initiate_time,omitempty"`
	StartTime    float64     `json:"start_time,omitempty"`
	CompleteTime float64     `json:"complete_time,omitempty"`
	ClearTime    float64     `json:"clear_time,omitempty"`
	Detail       interface{} `json:"detail,omitempty"`
}
