package trace

import (
	"gitlab.com/akita/akita"
)

// HookPosTaskInitiate is triggered when the task is first generated
var HookPosTaskInitiate = &akita.HookPos{Name: "HookPosTaskInitiate"}

// HookPosTaskStart is triggered when the task starts to be processed
var HookPosTaskStart = &akita.HookPos{Name: "HookPosTaskInitiate"}

// HookPosTaskComplete is triggered when the task processing is completed
var HookPosTaskComplete = &akita.HookPos{Name: "HookPosTaskInitiate"}

// HookPosTaskClear is triggered when the task is out of scope
var HookPosTaskClear = &akita.HookPos{Name: "HookPosTaskInitiate"}

// A Hook can hook
type Hook struct {
	tracer Tracer
}

// NewHook returns a newly created hook, injecting dependency of Tracer
func NewHook(t Tracer) *Hook {
	return &Hook{
		tracer: t,
	}
}

// Func logs the task into the database
func (h *Hook) Func(ctx *akita.HookCtx) {
	switch ctx.Pos {
	case HookPosTaskInitiate:
		task := ctx.Item.(Task)
		h.tracer.CreateTask(&task)
	case HookPosTaskStart:
		task := ctx.Item.(Task)
		h.tracer.UpdateTimestamp(task.ID, "StartTime", float64(ctx.Now))
	case HookPosTaskComplete:
		task := ctx.Item.(Task)
		h.tracer.UpdateTimestamp(task.ID, "CompleteTime", float64(ctx.Now))
	case HookPosTaskClear:
		task := ctx.Item.(Task)
		h.tracer.UpdateTimestamp(task.ID, "ClearTime", float64(ctx.Now))
		h.tracer.SaveTask(task.ID)
	}
}
