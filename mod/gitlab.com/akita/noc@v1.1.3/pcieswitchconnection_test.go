package noc

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/akita/mock_akita"
)

var _ = Describe("PCIeSwitchConnection", func() {
	var (
		mockCtrl *gomock.Controller
		engine   *mock_akita.MockEngine
		port1    *mock_akita.MockPort
		port2    *mock_akita.MockPort
		conn     *PCIeSwitchConnection
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		engine = mock_akita.NewMockEngine(mockCtrl)
		port1 = mock_akita.NewMockPort(mockCtrl)
		port2 = mock_akita.NewMockPort(mockCtrl)

		conn = NewPCIeSwitchConnection(16, engine, 1)
		port1.EXPECT().SetConnection(conn)
		port2.EXPECT().SetConnection(conn)

		conn.PlugIn(port1)
		conn.PlugIn(port2)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should put request to virtual channel buffer", func() {
		req := akita.NewReqBase()
		req.SetSrc(port1)
		req.SetDst(port2)
		req.SetTrafficClass(9)

		engine.EXPECT().Schedule(gomock.AssignableToTypeOf(akita.TickEvent{}))

		conn.Send(req)

		Expect(conn.srcPortToPoolMap[port1].Pool[1].Buf).To(HaveLen(1))
	})

	It("should schedule transfer", func() {
		req1 := akita.NewReqBase()
		req1.SetSrc(port1)
		req1.SetDst(port2)
		req1.SetTrafficClass(0)
		conn.srcPortToPoolMap[port1].Pool[0].enqueue(req1)

		req2 := akita.NewReqBase()
		req2.SetSrc(port1)
		req2.SetDst(port2)
		req2.SetTrafficClass(1)
		conn.srcPortToPoolMap[port1].Pool[1].enqueue(req2)

		req3 := akita.NewReqBase()
		req3.SetSrc(port2)
		req3.SetDst(port1)
		req3.SetTrafficClass(0)
		conn.srcPortToPoolMap[port2].Pool[0].enqueue(req3)

		req4 := akita.NewReqBase()
		req4.SetSrc(port2)
		req4.SetDst(port1)
		req4.SetTrafficClass(1)
		conn.srcPortToPoolMap[port2].Pool[1].enqueue(req4)

		conn.lastSelectedSrcBuf = 0
		conn.srcPortToPoolMap[port1].lastSelectedVC = 0
		conn.srcPortToPoolMap[port2].lastSelectedVC = 0

		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&TransferEvent{})).
			Do(func(evt *TransferEvent) {
				Expect(evt.req).To(BeIdenticalTo(req4))
			})

		conn.scheduleTransfer(11)

		Expect(conn.lastSelectedSrcBuf).To(Equal(1))
	})
})

var _ = Describe("PCIeSwitchConnection Connection Integration", func() {
	It("Should deliver", func() {
		engine := akita.NewSerialEngine()
		conn := NewPCIeSwitchConnection(16, engine, 1*akita.MHz)
		conn.NumLanes = 1
		agent := newSendRecvAgent("Agent", engine)
		conn.PlugIn(agent.out)

		agent.sendLeft = 50
		agent.ticker.TickLater(0)

		engine.Run()

		Expect(agent.recvCount).To(Equal(agent.sendCount))
	})
})
