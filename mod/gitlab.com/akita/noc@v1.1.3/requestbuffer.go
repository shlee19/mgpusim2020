package noc

import "gitlab.com/akita/akita"

type ReqBuffer struct {
	Capacity int
	Buf      []akita.Req
	vc       int
}

func (b *ReqBuffer) enqueue(req akita.Req) {
	if len(b.Buf) > b.Capacity {
		panic("Buffer overflow")
	}

	b.Buf = append(b.Buf, req)
}
