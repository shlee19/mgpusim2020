package noc

import "gitlab.com/akita/akita"

type TransferEvent struct {
	*akita.EventBase
	req akita.Req
	vc  int
}

func NewTransferEvent(
	time akita.VTimeInSec,
	handler akita.Handler,
	req akita.Req,
	vc int,
) *TransferEvent {
	evt := new(TransferEvent)
	evt.EventBase = akita.NewEventBase(time, handler)
	evt.req = req
	evt.vc = vc
	return evt

}
