module gitlab.com/akita/noc

require (
	9fans.net/go v0.0.2 // indirect
	github.com/alecthomas/gometalinter v3.0.0+incompatible // indirect
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d // indirect
	github.com/fatih/gomodifytags v1.0.1 // indirect
	github.com/fatih/structtag v1.1.0 // indirect
	github.com/golang/mock v1.2.0
	github.com/google/shlex v0.0.0-20181106134648-c34317bd91bf // indirect
	github.com/mdempsky/gocode v0.0.0-20190203001940-7fb65232883f // indirect
	github.com/onsi/ginkgo v1.7.0
	github.com/onsi/gomega v1.4.3
	github.com/rogpeppe/godef v1.1.1 // indirect
	github.com/sqs/goreturns v0.0.0-20181028201513-538ac6014518 // indirect
	github.com/tpng/gopkgs v0.0.0-20180428091733-81e90e22e204 // indirect
	github.com/zmb3/goaddimport v0.0.0-20170810013102-4ab94a07ab86 // indirect
	github.com/zmb3/gogetdoc v0.0.0-20190228002656-b37376c5da6a // indirect
	gitlab.com/akita/akita v1.4.0
	golang.org/x/tools v0.0.0-20191112005509-a3f652f18032 // indirect
	gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20191105091915-95d230a53780 // indirect
)

go 1.13
