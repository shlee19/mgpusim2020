package noc

import (
	"log"
	"reflect"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
)

//go:generate mockgen -destination "mock_akita_test.go" -package $GOPACKAGE -write_package_comment=false gitlab.com/akita/akita Port,Engine
func TestNOC(t *testing.T) {
	log.SetOutput(GinkgoWriter)
	RegisterFailHandler(Fail)
	RunSpecs(t, "NOC")
}

type trafficMsg struct {
	akita.MsgMeta
}

func (m *trafficMsg) Meta() *akita.MsgMeta {
	return &m.MsgMeta
}

type sendRecvAgent struct {
	akita.ComponentBase
	ticker *akita.Ticker

	out akita.Port

	sendLeft             int
	sendCount, recvCount int
}

func (a *sendRecvAgent) Handle(e akita.Event) error {
	switch e.(type) {
	case akita.TickEvent:
		msg := &trafficMsg{}
		meta := msg.Meta()
		meta.SendTime = e.Time()
		msg.Meta().Src = a.out
		msg.Meta().Dst = a.out
		msg.Meta().TrafficBytes = 64

		err := a.out.Send(msg)
		if err == nil {
			a.sendLeft--
			a.sendCount++
		}

		if a.sendLeft > 0 {
			a.ticker.TickLater(e.Time())
		}
	default:
		log.Panicf("cannot handle event of type %s", reflect.TypeOf(e))
	}
	return nil
}

func (a *sendRecvAgent) NotifyPortFree(now akita.VTimeInSec, port akita.Port) {
}

func (a *sendRecvAgent) NotifyRecv(now akita.VTimeInSec, port akita.Port) {
	port.Retrieve(now)
	a.recvCount++
}

func newSendRecvAgent(name string, engine akita.Engine) *sendRecvAgent {
	agent := new(sendRecvAgent)
	agent.ComponentBase = *akita.NewComponentBase(name)

	agent.ticker = akita.NewTicker(agent, engine, 1*akita.GHz)
	agent.out = akita.NewLimitNumMsgPort(agent, 4096)

	return agent
}
