package noc

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.com/akita/akita"
)

var _ = Describe("PCIeSwitchConnection", func() {
	var (
		mockCtrl *gomock.Controller
		engine   *MockEngine
		port1    *MockPort
		port2    *MockPort
		conn     *PCIeSwitchConnection
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		engine = NewMockEngine(mockCtrl)
		port1 = NewMockPort(mockCtrl)
		port2 = NewMockPort(mockCtrl)

		conn = NewPCIeSwitchConnection(16, engine, 1)
		port1.EXPECT().SetConnection(conn)
		port2.EXPECT().SetConnection(conn)

		conn.PlugIn(port1)
		conn.PlugIn(port2)
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should put request to virtual channel buffer", func() {
		msg := &trafficMsg{}
		msg.Src = port1
		msg.Dst = port2
		msg.TrafficClass = 9

		engine.EXPECT().Schedule(gomock.AssignableToTypeOf(akita.TickEvent{}))

		conn.Send(msg)

		Expect(conn.srcPortToPoolMap[port1].Pool[1].Buf).To(HaveLen(1))
	})

	It("should schedule transfer", func() {
		msg1 := &trafficMsg{}
		msg1.Src = port1
		msg1.Dst = port2
		msg1.TrafficClass = 0
		conn.srcPortToPoolMap[port1].Pool[0].enqueue(msg1)

		msg2 := &trafficMsg{}
		msg2.Src = port1
		msg2.Dst = port2
		msg2.TrafficClass = 1
		conn.srcPortToPoolMap[port1].Pool[1].enqueue(msg2)

		msg3 := &trafficMsg{}
		msg3.Src = port2
		msg3.Dst = port1
		msg3.TrafficClass = 0
		conn.srcPortToPoolMap[port2].Pool[0].enqueue(msg3)

		msg4 := &trafficMsg{}
		msg4.Src = port2
		msg4.Dst = port1
		msg4.TrafficClass = 1
		conn.srcPortToPoolMap[port2].Pool[1].enqueue(msg4)

		conn.lastSelectedSrcBuf = 0
		conn.srcPortToPoolMap[port1].lastSelectedVC = 0
		conn.srcPortToPoolMap[port2].lastSelectedVC = 0

		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&TransferEvent{})).
			Do(func(evt *TransferEvent) {
				Expect(evt.msg).To(BeIdenticalTo(msg4))
			})

		conn.scheduleTransfer(11)

		Expect(conn.lastSelectedSrcBuf).To(Equal(1))
	})
})

var _ = Describe("PCIeSwitchConnection Connection Integration", func() {
	It("Should deliver", func() {
		engine := akita.NewSerialEngine()
		conn := NewPCIeSwitchConnection(16, engine, 1*akita.MHz)
		conn.NumLanes = 1
		agent := newSendRecvAgent("Agent", engine)
		conn.PlugIn(agent.out)

		agent.sendLeft = 50
		agent.ticker.TickLater(0)

		engine.Run()

		Expect(agent.recvCount).To(Equal(agent.sendCount))
	})
})
