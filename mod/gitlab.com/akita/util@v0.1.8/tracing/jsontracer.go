package tracing

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/rs/xid"
	"github.com/tebeka/atexit"
)

// JsonTracer can write tasks into json format.
type JsonTracer struct {
	w             io.Writer
	lock          sync.Mutex
	firstTask     bool
	inflightTasks map[string]Task
}

// StartTask records the start of a task
func (t *JsonTracer) StartTask(task Task) {
	t.lock.Lock()
	t.inflightTasks[task.ID] = task
	t.lock.Unlock()
}

// StepTask records the moment that a task reaches a milestone
func (t *JsonTracer) StepTask(task Task) {
	// Do nothing right now
}

// EndTask records the time that a task is completed.
func (t *JsonTracer) EndTask(task Task) {
	t.lock.Lock()
	originalTask, ok := t.inflightTasks[task.ID]
	if !ok {
		t.lock.Unlock()
		return
	}
	originalTask.EndTime = task.EndTime

	delete(t.inflightTasks, task.ID)
	t.lock.Unlock()

	if t.firstTask {
		t.firstTask = false
	} else {
		t.w.Write([]byte(",\n"))
	}

	b, err := json.Marshal(originalTask)
	if err != nil {
		panic(err)
	}
	t.w.Write(b)
}

func (t *JsonTracer) finish() {
	t.w.Write([]byte("\n]"))
}

// NewJsonTracer creates a new JsonTracer, injecting a writer
// as dependency.
func NewJsonTracer() *JsonTracer {
	filename := xid.New().String() + ".json"
	f, err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Recodring tasks in %s\n", filename)

	f.Write([]byte("[\n"))

	t := &JsonTracer{
		w:             f,
		firstTask:     true,
		inflightTasks: make(map[string]Task),
	}

	atexit.Register(t.finish)

	return t
}
