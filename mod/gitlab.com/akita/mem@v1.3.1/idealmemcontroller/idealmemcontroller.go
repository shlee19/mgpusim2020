package idealmemcontroller

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/util/tracing"
)

type readRespondEvent struct {
	*akita.EventBase

	req *mem.ReadReq
}

func newReadRespondEvent(time akita.VTimeInSec, handler akita.Handler,
	req *mem.ReadReq,
) *readRespondEvent {
	return &readRespondEvent{akita.NewEventBase(time, handler), req}
}

type writeRespondEvent struct {
	*akita.EventBase

	req *mem.WriteReq
}

func newWriteRespondEvent(time akita.VTimeInSec, handler akita.Handler,
	req *mem.WriteReq,
) *writeRespondEvent {
	return &writeRespondEvent{akita.NewEventBase(time, handler), req}
}

// An Comp is an ideal memory controller that can perform read and write
//
// Ideal memory controller always respond to the request in a fixed number of
// cycles. There is no limitation on the concurrency of this unit.
type Comp struct {
	*akita.ComponentBase

	Storage *mem.Storage

	Freq             akita.Freq
	Latency          int
	Engine           akita.Engine
	AddressConverter AddressConverter

	ToTop akita.Port
}

// NotifyPortFree of a Comp does not do anything
func (c *Comp) NotifyPortFree(now akita.VTimeInSec, port akita.Port) {
	// Do nothing
}

// NotifyRecv triggers the Comp to retrieve requests from the port.
func (c *Comp) NotifyRecv(now akita.VTimeInSec, port akita.Port) {
	req := port.Retrieve(now)
	akita.ProcessMsgAsEvent(req, c.Engine, c.Freq)
}

// Handle defines how the Comp handles event
func (c *Comp) Handle(e akita.Event) error {
	switch e := e.(type) {
	case *mem.ReadReq:
		return c.handleReadReq(e)
	case *mem.WriteReq:
		return c.handleWriteReq(e)
	case *readRespondEvent:
		return c.handleReadRespondEvent(e)
	case *writeRespondEvent:
		return c.handleWriteRespondEvent(e)
	default:
		log.Panicf("cannot handle event of %s", reflect.TypeOf(e))
	}
	return nil
}

func (c *Comp) handleReadReq(req *mem.ReadReq) error {
	now := req.Time()
	timeToSchedule := c.Freq.NCyclesLater(c.Latency, req.RecvTime)
	respondEvent := newReadRespondEvent(timeToSchedule, c, req)
	c.Engine.Schedule(respondEvent)

	tracing.TraceReqReceive(req, now, c)

	return nil
}

func (c *Comp) handleWriteReq(req *mem.WriteReq) error {
	now := req.Time()
	timeToSchedule := c.Freq.NCyclesLater(c.Latency, req.RecvTime)
	respondEvent := newWriteRespondEvent(timeToSchedule, c, req)
	c.Engine.Schedule(respondEvent)

	tracing.TraceReqReceive(req, now, c)

	return nil
}

func (c *Comp) handleReadRespondEvent(e *readRespondEvent) error {
	now := e.Time()
	req := e.req

	addr := req.Address
	if c.AddressConverter != nil {
		addr = c.AddressConverter.ConvertExternalToInternal(addr)
	}

	data, err := c.Storage.Read(addr, req.AccessByteSize)
	if err != nil {
		log.Panic(err)
	}

	rsp := mem.DataReadyRspBuilder{}.
		WithSendTime(now).
		WithSrc(c.ToTop).
		WithDst(req.Src).
		WithRspTo(req.ID).
		WithData(data).
		Build()

	networkErr := c.ToTop.Send(rsp)
	if networkErr != nil {
		retry := newReadRespondEvent(c.Freq.NextTick(now), c, req)
		c.Engine.Schedule(retry)
		return nil
	}

	tracing.TraceReqComplete(req, now, c)

	return nil
}

func (c *Comp) handleWriteRespondEvent(e *writeRespondEvent) error {
	now := e.Time()
	req := e.req

	rsp := mem.WriteDoneRspBuilder{}.
		WithSendTime(now).
		WithSrc(c.ToTop).
		WithDst(req.Src).
		WithRspTo(req.ID).
		Build()

	networkErr := c.ToTop.Send(rsp)
	if networkErr != nil {
		retry := newWriteRespondEvent(c.Freq.NextTick(now), c, req)
		c.Engine.Schedule(retry)
		return nil
	}

	addr := req.Address

	if c.AddressConverter != nil {
		addr = c.AddressConverter.ConvertExternalToInternal(addr)
	}

	if req.DirtyMask == nil {
		err := c.Storage.Write(addr, req.Data)
		if err != nil {
			log.Panic(err)
		}
	} else {
		data, err := c.Storage.Read(addr, uint64(len(req.Data)))
		if err != nil {
			panic(err)
		}
		for i := 0; i < len(req.Data); i++ {
			if req.DirtyMask[i] == true {
				data[i] = req.Data[i]
			}
		}
		err = c.Storage.Write(addr, data)
		if err != nil {
			panic(err)
		}
	}

	tracing.TraceReqComplete(req, now, c)

	return nil
}

// New creates a new ideal memory controller
func New(
	name string,
	engine akita.Engine,
	capacity uint64,
) *Comp {
	c := new(Comp)
	c.ComponentBase = akita.NewComponentBase(name)
	c.Engine = engine

	c.Freq = 1 * akita.GHz
	c.Latency = 100

	c.Storage = mem.NewStorage(capacity)
	//c.ToTop = akita.NewLimitNumMsgPort(c, 1)
	c.ToTop = akita.NewLimitNumMsgPort(c, 40960000)
	return c
}
