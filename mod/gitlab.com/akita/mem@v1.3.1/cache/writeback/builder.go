package writeback

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
)

// A Builder can build writeback caches
type Builder struct {
	Engine           akita.Engine
	LowModuleFinder  cache.LowModuleFinder
	CacheName        string
	WayAssociativity int
	BlockSize        int
	ByteSize         uint64
	NumMSHREntry     int
}

// Build creates a usable writeback cache.
func (b *Builder) Build() *Cache {
	vimctimFinder := cache.NewLRUVictimFinder()
	numSet := int(b.ByteSize / uint64(b.WayAssociativity*b.BlockSize))
	directory := cache.NewDirectory(
		numSet, b.WayAssociativity, b.BlockSize, vimctimFinder)

	mshr := cache.NewMSHR(b.NumMSHREntry)
	storage := mem.NewStorage(b.ByteSize)

	cache := NewWriteBackCache(
		b.CacheName, b.Engine, directory, mshr,
		b.LowModuleFinder, storage)

	return cache
}
