package tlb

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem/vm"
	"gitlab.com/akita/util/akitaext"
	"gitlab.com/akita/util/tracing"
)

type transaction struct {
	req         *vm.TranslationReq
	fetchBottom *vm.TranslationReq
}

type set struct {
	Blocks   []vm.Page
	LRUQueue []int
}

// A TLB is a cache that maintains some page information.
type TLB struct {
	*akitaext.TickingComponent

	TopPort     akita.Port
	BottomPort  akita.Port
	ControlPort akita.Port

	LowModule akita.Port

	numSets        int
	numWays        int
	pageSize       uint64
	numReqPerCycle int

	Sets []*set

	translating []transaction

	isBusy bool
}

// Reset sets all the entries int he TLB to be invalid
func (tlb *TLB) reset() {
	tlb.Sets = make([]*set, tlb.numSets)
	for i := 0; i < int(tlb.numSets); i++ {
		set := &set{}
		tlb.Sets[i] = set
		set.Blocks = make([]vm.Page, tlb.numWays)
		set.LRUQueue = make([]int, tlb.numWays)
		for j := 0; j < int(tlb.numWays); j++ {
			set.LRUQueue[j] = j
		}
	}
}

// Tick defines how TLB update states at each cycle
func (tlb *TLB) Tick(now akita.VTimeInSec) bool {
	madeProgress := false

	madeProgress = tlb.performCtrlReq(now) || madeProgress

	for i := 0; i < tlb.numReqPerCycle; i++ {
		madeProgress = tlb.parseBottom(now) || madeProgress
	}

	for i := 0; i < tlb.numReqPerCycle; i++ {
		madeProgress = tlb.lookup(now) || madeProgress
	}

	return madeProgress
}

func (tlb *TLB) lookup(now akita.VTimeInSec) bool {
	item := tlb.TopPort.Peek()
	if item == nil {
		return false
	}

	req := item.(*vm.TranslationReq)
	for i, set := range tlb.Sets {
		for j, block := range set.Blocks {
			if tlb.isHit(block, req) {
				rsp := vm.TranslationRspBuilder{}.
					WithSendTime(now).
					WithSrc(tlb.TopPort).
					WithDst(req.Src).
					WithRspTo(req.ID).
					WithPage(block).
					Build()
				err := tlb.TopPort.Send(rsp)
				if err != nil {
					return false
				}

				tlb.visit(i, j)
				tlb.TopPort.Retrieve(now)

				tracing.TraceReqReceive(req, now, tlb)
				tracing.TraceReqComplete(req, now, tlb)

				return true
			}
		}
	}

	fetchBottom := vm.TranslationReqBuilder{}.
		WithSendTime(now).
		WithSrc(tlb.BottomPort).
		WithDst(tlb.LowModule).
		WithPID(req.PID).
		WithVAddr(req.VAddr).
		WithGPUID(req.GPUID).
		Build()
	err := tlb.BottomPort.Send(fetchBottom)
	if err != nil {
		return false
	}

	trans := transaction{
		req:         req,
		fetchBottom: fetchBottom,
	}
	tlb.translating = append(tlb.translating, trans)
	tlb.TopPort.Retrieve(now)

	tracing.TraceReqReceive(req, now, tlb)
	tracing.TraceReqInitiate(fetchBottom, now, tlb,
		tracing.MsgIDAtReceiver(req, tlb))

	return true
}

func (tlb *TLB) parseBottom(now akita.VTimeInSec) bool {
	item := tlb.BottomPort.Peek()
	if item == nil {
		return false
	}

	rsp := item.(*vm.TranslationRsp)
	page := rsp.Page

	trans, transIndex := tlb.findTransaction(rsp)
	rspToTop := vm.TranslationRspBuilder{}.
		WithSendTime(now).
		WithSrc(tlb.TopPort).
		WithDst(trans.req.Src).
		WithRspTo(trans.req.ID).
		WithPage(page).
		Build()
	rspToTop.Page = page
	err := tlb.TopPort.Send(rspToTop)
	if err != nil {
		return false
	}

	tlb.translating = append(
		tlb.translating[:transIndex], tlb.translating[transIndex+1:]...)
	setID := int(page.VAddr / uint64(tlb.pageSize) % uint64(tlb.numSets))
	set := tlb.Sets[setID]
	wayID := set.LRUQueue[0]
	set.Blocks[wayID] = page
	tlb.visit(setID, wayID)
	tlb.BottomPort.Retrieve(now)

	tracing.TraceReqFinalize(trans.fetchBottom, now, tlb)
	tracing.TraceReqComplete(trans.req, now, tlb)

	return true
}

func (tlb *TLB) performCtrlReq(now akita.VTimeInSec) bool {
	// item := tlb.ControlPort.Peek()
	// if item == nil {
	// 	return false
	// }

	// req := item.(vm.PTEInvalidationReq)
	// rsp := vm.NewInvalidationCompleteRsp(
	// 	now, tlb.ControlPort, req.Src(), req.GetID())
	// err := tlb.ControlPort.Send(*rsp)
	// if err != nil {
	// 	return false
	// }

	// tlb.ControlPort.Retrieve(now)
	// for _, set := range tlb.Sets {
	// 	for wayID, block := range set.Blocks {
	// 		if block.PID != req.PID {
	// 			continue
	// 		}

	// 		for _, addr := range req.Vaddr {
	// 			if block.VAddr == addr {
	// 				set.Blocks[wayID].Valid = false
	// 			}
	// 		}
	// 	}
	// }

	return false
}

func (tlb *TLB) isHit(block vm.Page, req *vm.TranslationReq) bool {
	if !block.Valid {
		return false
	}

	if block.PID != req.PID {
		return false
	}

	if block.VAddr != req.VAddr {
		return false
	}

	return true
}

func (tlb *TLB) visit(setID, wayID int) {
	set := tlb.Sets[setID]
	for i, b := range set.LRUQueue {
		if b == wayID {
			set.LRUQueue = append(set.LRUQueue[:i], set.LRUQueue[i+1:]...)
			set.LRUQueue = append(set.LRUQueue, b)
			return
		}
	}
}

func (tlb *TLB) findTransaction(
	rsp *vm.TranslationRsp,
) (transaction, int) {
	for index, trans := range tlb.translating {
		if trans.fetchBottom.ID == rsp.RespondTo {
			return trans, index
		}
	}
	panic("transaction not found")
}
