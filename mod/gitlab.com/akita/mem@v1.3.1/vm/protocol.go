// Package vm provides the models for address translations
package vm

import (
	"github.com/rs/xid"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/util/ca"
)

// A TranslationReq asks the receiver component to translate the request.
type TranslationReq struct {
	akita.MsgMeta
	VAddr uint64
	PID   ca.PID
	GPUID uint64
}

// Meta returns the meta data associated with the message.
func (r *TranslationReq) Meta() *akita.MsgMeta {
	return &r.MsgMeta
}

// TranslationReqBuilder can build translation requests
type TranslationReqBuilder struct {
	sendTime akita.VTimeInSec
	src, dst akita.Port
	vAddr    uint64
	pid      ca.PID
	gpuID    uint64
}

// WithSendTime sets the send time of the request to build.:w
func (b TranslationReqBuilder) WithSendTime(
	t akita.VTimeInSec,
) TranslationReqBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the request to build.
func (b TranslationReqBuilder) WithSrc(src akita.Port) TranslationReqBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the request to build.
func (b TranslationReqBuilder) WithDst(dst akita.Port) TranslationReqBuilder {
	b.dst = dst
	return b
}

// WithVAddr sets the virtual address of the request to build.
func (b TranslationReqBuilder) WithVAddr(vAddr uint64) TranslationReqBuilder {
	b.vAddr = vAddr
	return b
}

// WithPID sets the virtual address of the request to build.
func (b TranslationReqBuilder) WithPID(pid ca.PID) TranslationReqBuilder {
	b.pid = pid
	return b
}

// WithGPUID sets the GPU ID of the request to build.
func (b TranslationReqBuilder) WithGPUID(gpuID uint64) TranslationReqBuilder {
	b.gpuID = gpuID
	return b
}

// Build creats a new TranslationReq
func (b TranslationReqBuilder) Build() *TranslationReq {
	r := &TranslationReq{}
	r.ID = xid.New().String()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime
	r.VAddr = b.vAddr
	r.PID = b.pid
	r.GPUID = b.gpuID
	return r
}

// A TranslationRsp is the respond for a TranslationReq. It carries the physical
// address.
type TranslationRsp struct {
	akita.MsgMeta
	RespondTo string // The ID of the request it replies
	Page      Page
}

// Meta returns the meta data associated with the message.
func (r *TranslationRsp) Meta() *akita.MsgMeta {
	return &r.MsgMeta
}

// TranslationRspBuilder can build translation requests
type TranslationRspBuilder struct {
	sendTime akita.VTimeInSec
	src, dst akita.Port
	rspTo    string
	page     Page
}

// WithSendTime sets the send time of the message to build.
func (b TranslationRspBuilder) WithSendTime(
	t akita.VTimeInSec,
) TranslationRspBuilder {
	b.sendTime = t
	return b
}

// WithSrc sets the source of the respond to build.
func (b TranslationRspBuilder) WithSrc(src akita.Port) TranslationRspBuilder {
	b.src = src
	return b
}

// WithDst sets the destination of the respond to build.
func (b TranslationRspBuilder) WithDst(dst akita.Port) TranslationRspBuilder {
	b.dst = dst
	return b
}

// WithRspTo sets the request ID of the respond to build.
func (b TranslationRspBuilder) WithRspTo(rspTo string) TranslationRspBuilder {
	b.rspTo = rspTo
	return b
}

// WithPage sets the page of the respond to build.
func (b TranslationRspBuilder) WithPage(page Page) TranslationRspBuilder {
	b.page = page
	return b
}

// Build creats a new TranslationRsp
func (b TranslationRspBuilder) Build() *TranslationRsp {
	r := &TranslationRsp{}
	r.ID = xid.New().String()
	r.Src = b.src
	r.Dst = b.dst
	r.SendTime = b.sendTime
	r.RespondTo = b.rspTo
	r.Page = b.page
	return r
}

// TODO (Yifan): Refactor the following messages.

//A TLBFullInvalidate asks the receiver TLB to invalidate all its entries

type PTEInvalidationReq struct {
	akita.MsgMeta
	PID   ca.PID
	Vaddr []uint64
}

// Meta returns the meta data associated with the message.
func (m *PTEInvalidationReq) Meta() *akita.MsgMeta {
	return &m.MsgMeta
}

type InvalidationCompleteRsp struct {
	akita.MsgMeta

	RespondTo        string // The ID of the request it replies
	InvalidationDone bool
}

// Meta returns the meta data associated with the message.
func (m *InvalidationCompleteRsp) Meta() *akita.MsgMeta {
	return &m.MsgMeta
}

type TLBFullInvalidateReq struct {
	akita.MsgMeta
	PID ca.PID
}

// Meta returns the meta data associated with the message.
func (m *TLBFullInvalidateReq) Meta() *akita.MsgMeta {
	return &m.MsgMeta
}

//A TLBPartialInvalidate asks the receiver TLB to invalidate the list of particular entries.
// The list can consist anyhere from 0 to N entries
type TLBPartialInvalidateReq struct {
	akita.MsgMeta

	VAddr []uint64
	PID   ca.PID
}

// Meta returns the meta data associated with the message.
func (m *TLBPartialInvalidateReq) Meta() *akita.MsgMeta {
	return &m.MsgMeta
}

type PageMigrationReqToDriver struct {
	akita.MsgMeta

	VAddr           uint64
	PhysicalAddress uint64
	PageSize        uint64
	PID             ca.PID
	RequestingGPU   uint64
}

// Meta returns the meta data associated with the message.
func (m *PageMigrationReqToDriver) Meta() *akita.MsgMeta {
	return &m.MsgMeta
}

type PageMigrationRspFromDriver struct {
	akita.MsgMeta

	Vaddr             uint64
	MigrationComplete bool
}

// Meta returns the meta data associated with the message.
func (m *PageMigrationRspFromDriver) Meta() *akita.MsgMeta {
	return &m.MsgMeta
}

func NewInvalidationCompleteRsp(
	time akita.VTimeInSec,
	src, dst akita.Port,
	respondTo string,
) *InvalidationCompleteRsp {
	req := new(InvalidationCompleteRsp)

	req.SendTime = time
	req.Src = src
	req.Dst = dst

	req.RespondTo = respondTo

	return req
}

func NewPTEInvalidationReq(
	time akita.VTimeInSec,
	src, dst akita.Port,
	pid ca.PID,
	vAddr []uint64,
) *PTEInvalidationReq {
	req := new(PTEInvalidationReq)

	req.SendTime = time
	req.Src = src
	req.Dst = dst

	req.Vaddr = vAddr
	req.PID = pid

	return req
}

func NewPageMigrationReqToDriver(
	time akita.VTimeInSec,
	src, dst akita.Port,
) *PageMigrationReqToDriver {
	req := new(PageMigrationReqToDriver)

	req.SendTime = time
	req.Src = src
	req.Dst = dst

	return req
}

func NewPageMigrationRspFromDriver(
	time akita.VTimeInSec,
	src, dst akita.Port,
) *PageMigrationRspFromDriver {
	req := new(PageMigrationRspFromDriver)

	req.SendTime = time
	req.Src = src
	req.Dst = dst

	return req
}
