module gitlab.com/akita/mem

require (
	9fans.net/go v0.0.2 // indirect
	github.com/alecthomas/gometalinter v3.0.0+incompatible // indirect
	github.com/alecthomas/units v0.0.0-20190924025748-f65c72e2690d // indirect
	github.com/fatih/gomodifytags v1.0.1 // indirect
	github.com/fatih/structtag v1.1.0 // indirect
	github.com/golang/mock v1.3.1
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/google/shlex v0.0.0-20181106134648-c34317bd91bf // indirect
	github.com/mdempsky/gocode v0.0.0-20190203001940-7fb65232883f // indirect
	github.com/onsi/ginkgo v1.8.0
	github.com/onsi/gomega v1.5.0
	github.com/rogpeppe/godef v1.1.1 // indirect
	github.com/rs/xid v1.2.1
	github.com/sqs/goreturns v0.0.0-20181028201513-538ac6014518 // indirect
	github.com/tpng/gopkgs v0.0.0-20180428091733-81e90e22e204 // indirect
	github.com/zmb3/goaddimport v0.0.0-20170810013102-4ab94a07ab86 // indirect
	github.com/zmb3/gogetdoc v0.0.0-20190228002656-b37376c5da6a // indirect
	gitlab.com/akita/akita v1.4.0
	gitlab.com/akita/util v0.1.8
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect
	golang.org/x/tools v0.0.0-20191028215554-80f3f9ca0853 // indirect
	gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20180810215634-df19058c872c // indirect
)

go 1.13
