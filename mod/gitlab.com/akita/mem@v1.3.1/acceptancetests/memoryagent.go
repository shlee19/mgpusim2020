package acceptancetests

import (
	"encoding/binary"
	"log"
	"math/rand"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
)

// A MemAccessAgent is a Component that can help testing the cache and the the
// memory controllers by generating a large number of read and write requests.
type MemAccessAgent struct {
	*akita.ComponentBase
	*akita.Ticker

	engine     akita.Engine
	LowModule  akita.Port
	Freq       akita.Freq
	MaxAddress uint64

	WriteLeft       int
	ReadLeft        int
	KnownMemValue   map[uint64][]uint32
	PendingReadReq  map[string]*mem.ReadReq
	PendingWriteReq map[string]*mem.WriteReq

	ToMem akita.Port
}

// NotifyPortFree of the MemAccessAgent does nothing
func (a *MemAccessAgent) NotifyPortFree(now akita.VTimeInSec, port akita.Port) {
}

// NotifyRecv of the MemAccessAgent retrieves the request from the port.
func (a *MemAccessAgent) NotifyRecv(now akita.VTimeInSec, port akita.Port) {
	req := port.Retrieve(now)
	akita.ProcessMsgAsEvent(req, a.engine, a.Freq)
}

func (a *MemAccessAgent) checkReadResult(
	read *mem.ReadReq,
	dataReady *mem.DataReadyRsp,
) {
	found := false
	var i int
	var value uint32
	result := BytesToUint32(dataReady.Data)
	for i, value = range a.KnownMemValue[read.Address] {
		if value == result {
			found = true
			break
		}
	}

	if found {
		a.KnownMemValue[read.Address] = a.KnownMemValue[read.Address][i:]
	} else {
		log.Panicf("Mismatch when read 0x%X", read.Address)
	}
}

// Handle defines how the MemAccessAgent handles events.
func (a *MemAccessAgent) Handle(e akita.Event) error {
	a.ComponentBase.Lock()
	defer a.ComponentBase.Unlock()

	switch evt := e.(type) {
	case akita.TickEvent:
		if a.shouldRead() {
			a.doRead(e.Time())
		} else {
			a.doWrite(e.Time())
		}
	case *mem.WriteDoneRsp:
		//req := a.PendingWriteReq[evt.RespondTo]
		delete(a.PendingWriteReq, evt.RespondTo)
		//fmt.Printf("%.12f: Write 0x%X (0x%X) Compelete: %.12f\n",
		//	evt.RecvTime(), req.Address,
		//	req.Address&0xffffffffffffffc0,
		//	evt.RecvTime()-req.SendTime())
	case *mem.DataReadyRsp:
		req := a.PendingReadReq[evt.RespondTo]
		delete(a.PendingReadReq, evt.RespondTo)
		//fmt.Printf("%.12f: Read  0x%X (0x%X) Compelete: %.12f\n",
		//	evt.RecvTime(), req.Address,
		//	req.Address&0xffffffffffffffc0,
		//	evt.RecvTime()-req.SendTime())
		a.checkReadResult(req, evt)
	default:
		log.Panicf("cannot handle event of type %s", reflect.TypeOf(evt))
	}

	if a.ReadLeft > 0 || a.WriteLeft > 0 {
		a.TickLater(e.Time())
		//tick := akita.NewTickEvent(a.Freq.NextTick(e.Time()), a)
		//a.engine.Schedule(tick)
	}
	return nil
}

func (a *MemAccessAgent) shouldRead() bool {
	if len(a.KnownMemValue) == 0 {
		return false
	}

	if a.ReadLeft == 0 {
		return false
	}

	if a.WriteLeft == 0 {
		return true
	}

	dice := rand.Float64()
	return dice > 0.5
}

func (a *MemAccessAgent) doRead(now akita.VTimeInSec) {
	address := a.randomReadAddress()

	if a.isAddressInPendingReq(address) {
		return
	}

	readReq := mem.ReadReqBuilder{}.
		WithSrc(a.ToMem).
		WithDst(a.LowModule).
		WithAddress(address).
		WithByteSize(4).
		WithPID(1).
		Build()
	readReq.SendTime = now
	err := a.ToMem.Send(readReq)
	if err == nil {
		a.PendingReadReq[readReq.ID] = readReq
		a.ReadLeft--
		//fmt.Printf("%.12f: Read  0x%X (0x%X)\n", now, address, address&0xffffffffffffffc0)
	}
}

func (a *MemAccessAgent) randomReadAddress() uint64 {
	var addr uint64

	for {
		addr = rand.Uint64() % (a.MaxAddress / 4) * 4
		if _, written := a.KnownMemValue[addr]; written {
			return addr
		}
	}
}

func (a *MemAccessAgent) isAddressInPendingReq(addr uint64) bool {
	return a.isAddressInPendingWrite(addr) || a.isAddressInPendingRead(addr)
}

func (a *MemAccessAgent) isAddressInPendingWrite(addr uint64) bool {
	for _, write := range a.PendingWriteReq {
		if write.Address == addr {
			return true
		}
	}
	return false
}

func (a *MemAccessAgent) isAddressInPendingRead(addr uint64) bool {
	for _, read := range a.PendingReadReq {
		if read.Address == addr {
			return true
		}
	}
	return false
}

func Uint32ToBytes(data uint32) []byte {
	bytes := make([]byte, 4)
	binary.LittleEndian.PutUint32(bytes, data)
	return bytes
}

func BytesToUint32(data []byte) uint32 {
	a := uint32(0)
	a += uint32(data[0])
	a += uint32(data[1]) << 8
	a += uint32(data[2]) << 16
	a += uint32(data[3]) << 24
	return a
}

func (a *MemAccessAgent) doWrite(now akita.VTimeInSec) {
	address := rand.Uint64() % (a.MaxAddress / 4) * 4
	data := rand.Uint32()

	if a.isAddressInPendingReq(address) {
		return
	}

	writeReq := mem.WriteReqBuilder{}.
		WithSrc(a.ToMem).
		WithDst(a.LowModule).
		WithAddress(address).
		WithPID(1).
		WithData(Uint32ToBytes(data)).
		Build()
	writeReq.SendTime = now

	err := a.ToMem.Send(writeReq)
	if err == nil {
		a.WriteLeft--
		a.addKnownValue(address, data)
		a.PendingWriteReq[writeReq.ID] = writeReq
		//fmt.Printf("%.12f: Write 0x%X (0x%X)\n", now, address, address&0xffffffffffffffc0)
	}
}

func (a *MemAccessAgent) addKnownValue(address uint64, data uint32) {
	valueList, exist := a.KnownMemValue[address]
	if !exist {
		valueList = make([]uint32, 0)
		a.KnownMemValue[address] = valueList
	}
	valueList = append(valueList, data)
	a.KnownMemValue[address] = valueList
}

func NewMemAccessAgent(engine akita.Engine) *MemAccessAgent {
	componentBase := akita.NewComponentBase("Agent")
	agent := new(MemAccessAgent)
	agent.ComponentBase = componentBase
	agent.engine = engine
	agent.Freq = 1 * akita.GHz
	agent.Ticker = akita.NewTicker(agent, engine, agent.Freq)
	agent.ToMem = akita.NewLimitNumMsgPort(agent, 1)

	agent.ReadLeft = 10000
	agent.WriteLeft = 10000
	agent.KnownMemValue = make(map[uint64][]uint32)
	agent.PendingWriteReq = make(map[string]*mem.WriteReq)
	agent.PendingReadReq = make(map[string]*mem.ReadReq)

	return agent
}
